<?php 
/* For Navigation Menu */
$lang['Main Navigation']= "Navigation principale";
$lang['Dashboard']= "Tableau de bord";
$lang['Payment History']= "Historique de paiement";
$lang['Sales Panel']= "Panneau de vente";
$lang['Payment From Admin']= "Paiement de l'administrateur";

/* For Profile Section*/
$lang['Merchant']= "Marchande";
$lang['Profile']= "Profil";
$lang['Sign Out']= "Déconnexion";
$lang['My Profile']= "Mon profil";
$lang['View or modify information']= "Afficher ou modifier des informations";
$lang['Share With']= "Partager avec";
$lang['Details']= "Détails";
$lang['Change Password']= "Changer le mot de passe";
$lang['Bank Details']= "Coordonnées bancaires";
$lang['Email']= "Email";
$lang['Mobile']= "Mobile";
$lang['Qr Code']= "Qr Code";
$lang['Full Name']= "Nom complet";
$lang['Mobile Number']= "Numéro de portable";
$lang['Profile Image']= "Image de profil";
$lang['Country']= "Pays";
$lang['State']= "Etat";
$lang['City']= "Ville";
$lang['Zip Code']= "Code postal";
$lang['Address']= "Adresse";
$lang['Submit']= "Soumettre";
$lang['Reset']= "Réinitialiser";
$lang['Old Password']= "Ancien mot de passe";
$lang['New Password']= "Nouveau mot de passe";
$lang['Confirm New Password']= "Confirmer le nouveau mot de passe";
$lang['Account Holder Name']= "Nom du titulaire du compte";
$lang['Bank Name']= "Nom du titulaire du compte";
$lang['Swift Code']= "Code rapide";
$lang['Routing Number']= "Numéro de routage";
$lang['Account Number']= "Numéro de compte";
$lang['Save Changes']= "Sauvegarder les modifications";

  /* For Payment History Section */
$lang['Transaction ID']= "Identifiant de transaction";
$lang['Amount']= "Montant";
$lang['Customer ID']= "N ° de client";
$lang['Customer Name']= "Nom du client";
$lang['Payment Status']= "Statut de paiement";
$lang['Payment Date']= "Date de paiement";
$lang['Sales History']= "Histoire des ventes";

?>