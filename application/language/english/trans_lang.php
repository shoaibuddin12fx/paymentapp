<?php 
/* For Navigation Menu */
$lang['Main Navigation']= "Main Navigation";
$lang['Dashboard']= "Dashboard";
$lang['Payment History']= "Payment History";
$lang['Sales Panel']= "Sales Panel";
$lang['Payment From Admin']= "Payment From Admin";

/* For Profile Section*/
$lang['Merchant']= "Merchant";
$lang['Profile']= "Profile";
$lang['Sign Out']= "Sign Out";
$lang['My Profile']= "My Profile";
$lang['View or modify information']= "View or modify information";
$lang['Share With']= "Share With";
$lang['Details']= "Details";
$lang['Change Password']= "Change Password";
$lang['Bank Details']= "Bank Details";
$lang['Email']= "Email";
$lang['Mobile']= "Mobile";
$lang['Qr Code']= "Qr Code";
$lang['Full Name']= "Full Name";
$lang['Mobile Number']= "Mobile Number";
$lang['Profile Image']= "Profile Image";
$lang['Country']= "Country";
$lang['State']= "State";
$lang['City']= "City";
$lang['Zip Code']= "Zip Code";
$lang['Address']= "Address";
$lang['Submit']= "Submit";
$lang['Reset']= "Reset";
$lang['Old Password']= "Old Password";
$lang['New Password']= "New Password";
$lang['Confirm New Password']= "Confirm New Password";
$lang['Account Holder Name']= "Account Holder Name";
$lang['Bank Name']= "Bank Name";
$lang['Swift Code']= "Swift Code";
$lang['Routing Number']= "Routing Number";
$lang['Account Number']= "Account Number";
$lang['Save Changes']= "Save Changes";

 /* For Payment History Section */
$lang['Transaction ID']= "Transaction ID";
$lang['Amount']= "Amount";
$lang['Customer ID']= "Customer ID";
$lang['Customer Name']= "Customer Name";
$lang['Payment Status']= "Payment Status";
$lang['Payment Date']= "Payment Date";
$lang['Sales History']= "Sales History";
?>