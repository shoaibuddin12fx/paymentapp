<?php 
/* For Navigation Menu */
$lang['Main Navigation']= "الملاحة الرئيسية";
$lang['Dashboard']= "لوحة القيادة";
$lang['Payment History']= "تاريخ الدفع";
$lang['Sales Panel']= "لوحة المبيعات";
$lang['Payment From Admin']= "الدفع من المسؤول";

/* For Profile Section*/
$lang['Merchant']= "تاجر";
$lang['Profile']= "الملف الشخصي";
$lang['Sign Out']= "خروج";
$lang['My Profile']= "ملفي";
$lang['View or modify information']= "عرض أو تعديل المعلومات";
$lang['Share With']= "شارك مع";
$lang['Details']= "تفاصيل";
$lang['Change Password']= "غير كلمة السر";
$lang['Bank Details']= "تفاصيل البنك";
$lang['Email']= "البريد الإلكتروني";
$lang['Mobile']= "التليفون المحمول";
$lang['Qr Code']= "رمز الاستجابة السريعة";
$lang['Full Name']= "الاسم الكامل";
$lang['Mobile Number']= "رقم الهاتف المحمول";
$lang['Profile Image']= "صورة الملف الشخصي";
$lang['Country']= "بلد";
$lang['State']= "حالة";
$lang['City']= "مدينة";
$lang['Zip Code']= "الرمز البريدي";
$lang['Address']= "عنوان";
$lang['Submit']= "خضع";
$lang['Reset']= "إعادة تعيين";
$lang['Old Password']= "كلمة المرور القديمة";
$lang['New Password']= "كلمة السر الجديدة";
$lang['Confirm New Password']= "تأكيد كلمة المرور الجديدة";
$lang['Account Holder Name']= "اسم صاحب الحساب";
$lang['Bank Name']= "اسم البنك";
$lang['Swift Code']= "رمز السرعة";
$lang['Routing Number']= "رقم التوصيل";
$lang['Account Number']= "رقم حساب";
$lang['Save Changes']= "حفظ التغييرات";

 /* For Payment History Section */
$lang['Transaction ID']= "معرف المعاملة";
$lang['Amount']= "كمية";
$lang['Customer ID']= "هوية الزبون";
$lang['Customer Name']= "اسم الزبون";
$lang['Payment Status']= "حالة السداد";
$lang['Payment Date']= "موعد الدفع";
$lang['Sales History']= "تاريخ المبيعات";

 

?>