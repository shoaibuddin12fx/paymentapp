<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/loader.css"" type="text/css"/>

<div class="content-wrapper">
<div class="loader">
 <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
</div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> User Management
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
				<?php if(base64_decode(base64_decode(base64_decode(base64_decode($this->uri->segment(2)))))==1){?>
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>addNew"><i class="fa fa-plus"></i> Add New</a>
                <?php }?></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 userlt">
              <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Users List</h3>
                    <div class="box-tools">
                        <form action="<?php if(base64_decode(base64_decode(base64_decode(base64_decode($this->uri->segment(2)))))==1){echo base_url('merchantListing'); }elseif(base64_decode(base64_decode(base64_decode(base64_decode($this->uri->segment(2)))))==2){ echo base_url('customerListing'); }else{echo base_url('customerListing');}?>" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
				  <table class="table table-hover table-bordered table-striped">
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Role</th>
                        <th>Created On</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    <?php
                    if(!empty($userRecords))
                    {
                        foreach($userRecords as $record)
                        {
                    ?>
                    <tr>
					<?php //print_r($record); ?>
                        <td><?php echo $record->name ?></td>
                        <td><?php echo $record->email ?></td>
                        <td><?php echo $record->mobile ?></td>
                        <td><?php echo $record->role ?></td>                 
                        <td><?php echo date("d-m-Y", strtotime($record->createdDtm)) ?></td>
                        <td class="text-center">
                            <a class="btn btn-sm btn-primary" href="<?= base_url().'login-history/'.$record->userId; ?>" title="Login history"><i class="fa fa-history"></i></a>  
                            <?php if($record->role!='Customer'){?><a class="btn btn-sm btn-info" href="<?php echo base_url().'editOld/'.$record->userId; ?>" title="Edit"><i class="fa fa-pencil"></i></a> <?php }?>
                            <a class="btn btn-sm btn-danger deleteUser" href="#" data-userid="<?php echo $record->userId; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                        <?php if($record->isDeactivated==0){							
							echo '<a class="btn btn-sm btn-success deactivateUser" href="#" data-userid="'.$record->userId.'" data-email="'.$record->email.'" data-name="'.$record->name.'" title="Deactivate" data-request_type="1">Deactivate</a>';
						}else{ 
						echo '<a class="btn btn-sm btn-success deactivateUser" href="#" data-userid="'.$record->userId.'" data-email="'.$record->email.'" data-name="'.$record->name.'" title="Activate" data-request_type="0">Activate</a>';
						 } ?>
                        </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href; 
            var value = link.substring(link.lastIndexOf('/') + 1);			
            jQuery("#searchList").attr("action", baseURL + "<?php echo $this->uri->segment(1)."/"?>" + value);
            jQuery("#searchList").submit();
        });
		
		jQuery(document).on("click", ".deactivateUser", function(){
		var userId = $(this).data("userid"),
			hitURL = baseURL + "DeactivateActivateUser",
			request_type = $(this).data("request_type"),
			email = $(this).data("email"),
			name = $(this).data("name"),
			currentRow = $(this);
		if(request_type==1)
		var confirmation = confirm("Do you want to deactivate this user?");
		else
		var confirmation = confirm("Do you want to activate this user?");	
		if(confirmation)
		{
			$('.loader').show();
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { userId : userId,request_type : request_type, email : email, name : name } 
			}).done(function(data){	
               			
				if(data.status == "activated") 
				{ 
			       $('.loader').hide();	
				  alert("User Activated"); 
				  location.reload();
				}
				else if(data.status == "deactivated") 
				{ 
			        $('.loader').hide();
					alert("User Deactivated"); 
					location.reload();
				}
				else { jQuery('.loader').hide();alert("Access denied..!"); }
			});
		}
	});
    });
</script>
