<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Payment History
        <!--<small>Add, Edit, Delete</small> -->
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
					Manage Transactions
					</h3>
                   
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table id="manageTransaction" class="table table-bordered table-striped dataTable " role="grid" aria-describedby="example1_info">
				  <thead>
                    <tr>
					    <th class="price-list-sno">#</th>
                        <th>Transaction ID</th>
                        <th>Amount</th>                                            
						<th>Merchant Email</th>
						<th>Merchant Name</th>	                       
                        <th>Paid Amount</th>
						
											      					
                    </tr>
					</thead>
					<tbody>
                    <?php
                    if(!empty($overviewDailyPayment))
                    { $i=0;
						 if(count($overviewDailyPayment)):$inc =1;
                        foreach($overviewDailyPayment as $record)
                        { 
                    ?>
					
                    <tr>
					    <td><?php echo $inc; ?></td>
                        <td><?php echo $record->paidByAdminTransactionId ?></td>
                        <td><?php echo $record->amount ?></td>                                        
						<td><?php echo $record->email ?></td>
						<td><?php echo $record->name ?></td>                  
						<td><?php echo $record->paidToMerchantAmount?></td>
						 
                        
                    </tr>
                    <?php
					$i++;
					$inc++;
					
                        }endif;
                    }
                    ?>
					</tbody>
                  </table>
                  
                </div><!-- /.box-body -->
               
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js" charset="utf-8"></script>

  <script>
  $(function(){
    $("#manageTransaction").dataTable();
  })
  </script>  
