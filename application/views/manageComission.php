<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Manage Comission
        <!--<small>Add, Edit, Delete</small> -->
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6 center-form">
              <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-body no-padding">
				<form action="<?php echo base_url('manageComission') ?>" method="post" id="editUser" role="form" enctype="multipart/form-data">
                  <table id="manageTransaction" class="table table-bordered table-striped dataTable " role="grid" aria-describedby="example1_info">
			
					<tbody>
                  
					
                    <tr>
					    <td>  <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="comission">Set Comission Percentage (%)</label>
                                        <select class="form-control" id="comission_percentage" name="comission_info">
                                            <option value="0">Select Percentage</option>
                                            <?php
                                           
                                                for($i=1;$i<=90;$i++)
                                                {
                                                    ?>
                                                    <option value="<?php echo $i; ?>" <?php if($manageComission->comission_percentage == $i) {echo "selected=selected";} ?>><?php echo $i ?></option>
                                                    <?php
                                                }
                                            
                                            ?>
                                        </select>
									<input type="hidden" name="id" value="<?php echo $manageComission->id ;?>" />
									<input type="hidden" name="type" value=1 />
						<div class="box-footer no-bck">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                        </div>
                                    </div>
                                </div> </td>
                        
                      
                    </tr>
                 
					</tbody>
                  </table>
                  </form>
                </div><!-- /.box-body -->
               
              </div><!-- /.box -->
            </div>
			<div class="col-xs-12 col-md-6 center-form"> 
			 <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?></div>
        </div>
    </section>
</div>

