<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> <?php echo $this->lang->line('Payment History'); ?>
        <!--<small>Add, Edit, Delete</small> -->
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                   
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">
					<?php if(base64_decode(base64_decode(base64_decode(base64_decode($this->uri->segment(2)))))==1) {echo $this->lang->line('Sales History');} else {echo "Paid By Admin";}?>
					</h3>
                   
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table id="paymentHistory" class="table table-bordered table-striped dataTable " role="grid" aria-describedby="example1_info">
				  <thead>
                    <tr>
					
					    <th class="price-list-sno">#</th>
                        <th><?php echo $this->lang->line('Transaction ID'); ?></th>
                        <th><?php echo $this->lang->line('Amount'); ?></th>
                        <!--<th><?php echo $this->lang->line('Customer ID'); ?></th> -->
                        <th><?php echo $this->lang->line('Customer Name'); ?></th>
                        <th><?php echo $this->lang->line('Payment Status'); ?></th>
                        <th><?php echo $this->lang->line('Payment Date'); ?></th>
              
						
                    </tr>
					</thead>
					<tbody>
                    <?php
                    if(!empty($paymentHistory))
                    {
						 if(count($paymentHistory)):$inc =1;
                        foreach($paymentHistory as $record)
                        { 
                    ?>
                    <tr>
					    <td><?php echo $inc; ?></td>
                        <td><?php echo $record->transaction_id ?></td>
                        <td><?php echo $record->amount ?></td>
                       <!-- <td><?php echo $record->customer_id ?></td> -->
                        <td><?php echo $record->customerName ?></td>
                        <td><?php if($record->payment_status==1){echo 'Success'; }else echo 'Failed'; ?></td>
                        <td><?php 
						if(base64_decode(base64_decode(base64_decode(base64_decode($this->uri->segment(2)))))==1) {echo date("m-d-Y", strtotime($record->payment_date_time));} else {echo $record->paid_by_admin_at;}
						 ?></td>
                      
                    </tr>
                    <?php
					$inc++;
                        }endif;
                    }
                    ?>
					</tbody>
                  </table>
                  
                </div><!-- /.box-body -->
               
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js" charset="utf-8"></script>

  <script>
  $(function(){
    $("#paymentHistory").dataTable();
  })
  </script>  
