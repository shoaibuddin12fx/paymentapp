<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Payment History
        <!--<small>Add, Edit, Delete</small> -->
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">
					Manage Transactions
					</h3>
                   
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table id="manageTransaction" class="table table-bordered table-striped dataTable " role="grid" aria-describedby="example1_info">
				  <thead>
                    <tr>
					
					    <th class="price-list-sno">#</th>
                        <th>Transaction ID</th>
                        <th>Amount</th>
                        <th>Customer Email</th>
                        <th>Customer Name</th>
                        <th>Payment Status</th>
						<th>Merchant Email</th>
						<th>Merchant Name</th>	
                        <th>Payment Date</th>
											      					
                    </tr>
					</thead>
					<tbody>
                    <?php
                    if(!empty($manageTransaction))
                    { $i=0;
						 if(count($manageTransaction)):$inc =1;
                        foreach($manageTransaction as $record)
                        { 
                    ?>
					
                    <tr>
					    <td><?php echo $inc; ?></td>
                        <td><?php echo $record->transaction_id ?></td>
                        <td><?php echo $record->amount ?></td>
                        <td><?php echo $record->customerEmail ?></td>
                        <td><?php echo $record->customerName ?></td>
                        <td><?php if($record->payment_status==1){echo 'Success'; }else {echo 'Failed';} ?></td>
						<td><?php echo $record->merchantEmail ?></td>
						<td><?php echo $record->merchantName ?></td>
                        <td><?php echo date("m-d-Y", strtotime($record->payment_date_time)) ?></td>
                      
                    </tr>
                    <?php
					$i++;
					$inc++;
					
                        }endif;
                    }
                    ?>
					</tbody>
                  </table>
                  
                </div><!-- /.box-body -->
               
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js" charset="utf-8"></script>

  <script>
  $(function(){
    $("#manageTransaction").dataTable();
  })
  </script>  
