<?php

$userId = $userInfo->userId;
$name = $userInfo->name;
$email = $userInfo->email;
$mobile = $userInfo->mobile;
$address = $userInfo->address;
$city = $userInfo->city;
$zipCode = $userInfo->zipCode;
$country = $userInfo->country;
$state = $userInfo->state;
$roleId = $userInfo->roleId;
$profile_pic = $userInfo->profile_image;
$role = $userInfo->role;
$qrFileName = $userInfo->qr_file_name;
$account_holder = $userInfo->account_holder;
$bankname = $userInfo->bankname;
$swiftcode = $userInfo->swiftcode;
$routingnumber = $userInfo->routingnumber;
$accountnumber = $userInfo->accountnumber;
$username = $userInfo->username;								
?>

<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-user-circle"></i> <?php echo $this->lang->line('My Profile')?>
        <small><?php echo $this->lang->line('View or modify information')?></small>
		
      </h1>
    </section>
    
    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-3">
              <!-- general form elements -->
                <div class="box box-warning">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="<?php if(empty($profile_pic)){echo base_url(); ?>assets/dist/img/<?php echo "avatar.png" ; }else {echo base_url(); ?>uploads/profile-pics/<?php echo $profile_pic ; }?>" alt="User profile picture">
                        <h3 class="profile-username text-center"><?= $username ?></h3>

                        <p class="text-muted text-center"><?= $role ?></p>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b><?php echo $this->lang->line('Email')?></b> <a class="pull-right"><?= $email ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo $this->lang->line('Mobile')?></b> <a class="pull-right"><?= $mobile ?></a>
                            </li>
                        </ul>                   
						<div class="profile_qr">
							<label for="mobile"><?php echo $this->lang->line('Qr Code')?></label>
							<img src="<?php echo base_url('uploads/qrcodes/').$qrFileName ?>" alt=""/>
							<input type="hidden" name="qr_file_name" value="<?php echo $qrFileName ?>">
						</div>
						<div class="share_link">
							<?php echo $this->lang->line('Share With')?>:- <br>
							<!--<a class="btn btn-social-icon btn-facebook">
							<span class="fa fa-facebook" data-sharer="facebook" data-hashtag="hashtag" data-url="<?php echo base_url('uploads/qrcodes/').$qrFileName ?>"></span>
							</a> -->
							<a class="btn btn-social-icon btn-whatsapp" target="_blank">
							<span class="fa fa-whatsapp" data-sharer="whatsapp" data-hashtag="hashtag" data-url="<?php echo base_url('uploads/qrcodes/').$qrFileName ?>"></span>
							</a>
							<!--<a class="btn btn-social-icon btn-pinterest">
							<span class="fa fa-pinterest" data-sharer="pinterest" data-hashtag="hashtag" data-url="<?php echo base_url('uploads/qrcodes/').$qrFileName ?>"></span>
							</a>
							<a class="btn btn-social-icon btn-twitter">
							<span class="fa fa-twitter" data-sharer="twitter" data-hashtag="hashtag" data-url="<?php echo base_url('uploads/qrcodes/').$qrFileName ?>"></span> -->
							</a>
							<a class="btn btn-social-icon btn-email">
							<span class="fa fa-email" data-sharer="email" data-hashtag="hashtag" data-url="<?php echo base_url('uploads/qrcodes/').$qrFileName ?>">@</span>
							</a>
						   <!-- <a class="btn btn-social-icon btn-reddit">
							<span class="fa fa-reddit" data-sharer="reddit" data-hashtag="hashtag" data-url="<?php echo base_url('uploads/qrcodes/').$qrFileName ?>"></span>
							</a>
							<a class="btn btn-social-icon btn-tumblr">
							<span class="fa fa-blogger" data-sharer="tumblr" data-hashtag="hashtag" data-url="<?php echo base_url('uploads/qrcodes/').$qrFileName ?>"></span>
							</a> -->
							<a class="btn btn-social-icon btn-facebook">
							<span class="fa fa-facebook" id="fb-share" data-url="<?php echo base_url('uploads/qrcodes/').$qrFileName ?>"></span></a>
						</div>
										
                                    
    
                    </div>
                </div>

            </div>

            <div class="col-md-5">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="<?= ($active == "details")? "active" : "" ?>"><a href="#details" data-toggle="tab"><?php echo $this->lang->line('Details')?></a></li>
                        <li class="<?= ($active == "changepass")? "active" : "" ?>"><a href="#changepass" data-toggle="tab"><?php echo $this->lang->line('Change Password')?></a></li> 
						<?php if($this->session->userdata('role') != 1)
                         { ?>
                        <li class="<?= ($active == "bankdetails")? "active" : "" ?>"><a href="#bankdetails" data-toggle="tab"><?php echo $this->lang->line('Bank Details')?></a></li>	
						 <?php }?>						
                    </ul>
                    <div class="tab-content">
                        <div class="<?= ($active == "details")? "active" : "" ?> tab-pane" id="details">
                            <form action="<?php echo base_url() ?>profileUpdate" method="post" id="editProfile" role="form" enctype="multipart/form-data"/>
                                <?php $this->load->helper('form'); ?>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-6">                                
                                            <div class="form-group">
                                                <label for="fname"><?php echo $this->lang->line('Full Name')?></label>
                                                <input type="text" class="form-control" id="fname" name="fname" placeholder="<?php echo $name; ?>" value="<?php echo set_value('fname', $name); ?>" maxlength="128" />
                                                <input type="hidden" value="<?php echo $userId; ?>" name="userId" id="userId" />    
                                            </div>
                                        </div>
										<div class="col-md-6">
                                            <div class="form-group">
                                                <label for="mobile"><?php echo $this->lang->line('Mobile Number')?></label>
                                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="<?php echo $mobile; ?>" value="<?php echo set_value('mobile', $mobile); ?>" maxlength="10">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        
                                    
                                    
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="email"><?php echo $this->lang->line('Email')?></label>
                                                <input type="text" class="form-control" id="email" name="email" placeholder="<?php echo $email; ?>" value="<?php echo set_value('email', $email); ?>">
                                            </div>
                                        </div>
										<div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="profilePic"><?php echo $this->lang->line('Profile Image')?></label>
                                        <input type="file" class="form-control " id="profile_pic" value="<?php //echo set_value('profile_pic', $profile_pic); ?>" name="profile_pic"/>
										<input type="hidden" value="<?php echo $profile_pic; ?>" name="pro_pic"/>
                                    </div>
                                    
                                </div>
                                    </div>
								<div class="row">
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="country"><?php echo $this->lang->line('Country')?></label>
                                        <input type="text" class="form-control required " id="country" value="<?php echo set_value('country', $country); ?>" name="country" maxlength="128">
                                    </div>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="state"><?php echo $this->lang->line('State')?></label>
                                        <input type="text" class="form-control required" value="<?php echo set_value('state', $state); ?>" id="state" name="state" maxlength="128">
                                    </div>
                                    
                                </div>
                                
								
                            </div>
							   <div class="row">
							   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="city"><?php echo $this->lang->line('City')?></label>
                                        <input type="text" class="form-control required " id="city" value="<?php echo set_value('city', $city); ?>" name="city" maxlength="128">
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Zip"><?php echo $this->lang->line('Zip Code')?></label>
                                        <input type="text" class="form-control required " id="zipCode" value="<?php echo set_value('zipCode', $zipCode); ?>" name="zipCode" maxlength="7">
                                    </div>
                                </div>
							<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address"><?php echo $this->lang->line('Address')?></label>
                                        <input type="text" class="form-control required " id="address" value="<?php echo set_value('address', $address); ?>" name="address" maxlength="128">
                                    </div>
                                </div>
                         
								</div>
                                </div><!-- /.box-body -->
                                <div class="box-footer">
                                    <input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('Submit')?>" />
                                    <input type="reset" class="btn btn-default" value="<?php echo $this->lang->line('Reset')?>" />
                                </div>
                            </form>
                        </div>
                        <div class="<?= ($active == "changepass")? "active" : "" ?> tab-pane" id="changepass">
                            <form role="form" action="<?php echo base_url() ?>changePassword" method="post">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="inputPassword1"><?php echo $this->lang->line('Old Password')?></label>
                                                <input type="password" class="form-control" id="inputOldPassword" placeholder="Old password" name="oldPassword" maxlength="20" required>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="inputPassword1"><?php echo $this->lang->line('New Password')?></label>
                                                <input type="password" class="form-control" id="inputPassword1" placeholder="New password" name="newPassword" maxlength="20" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="inputPassword2"><?php echo $this->lang->line('Confirm New Password')?></label>
                                                <input type="password" class="form-control" id="inputPassword2" placeholder="Confirm new password" name="cNewPassword" maxlength="20" required>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.box-body -->
            
                                <div class="box-footer">
                                    <input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('Submit')?>" />
                                    <input type="reset" class="btn btn-default" value="<?php echo $this->lang->line('Reset')?>" />
                                </div>
                            </form>
                        </div>  
                        <div class="<?= ($active == "bankdetails")? "active" : "" ?> tab-pane" id="bankdetails">
                            <form method="post" id="basic_info_frm">
                                                                           
                            <div class="box-body">
							<div class="successmess alert alert-success" style="display:none; text-transform: none;"> Saved successfully </div>
                               <div class="setting-page setting-bank-detl">
                               <div class="row">
							   <div class="bank col-md-6 form-group"><div class="label-field"><?php echo $this->lang->line('Account Holder Name')?></div>
										   <div class="field-input-blk">
										   <input name="holder_name" class="form-control" id="holder_name" placeholder="Account Holder Name" value="<?php echo $account_holder;?>" type="text"> 
										   <span class="error HolderFormFieldMessage"> Holder Name is required.</span>
										   <span id="acc_holder"> </span>
										   </div>
										   </div>
									 <div class="bank col-md-6 form-group"><div class="label-field"><?php echo $this->lang->line('Bank Name')?></div>
										   <div class="field-input-blk">
										   <input name="bankname" class="form-control" id="bankname" placeholder="Bank Name" value="<?php echo $bankname;?>" type="text"> 
										   <span class="error FormFieldMessageError1"> Bank Name is required.</span>
										   <span id="bank_name"></span>
										   </div>
										   </div>
                                </div> 
								<div class="row">
                                 <div class="bank  col-md-6 form-group"><div class="label-field"><?php echo $this->lang->line('Swift Code')?></div>
										   <div class="field-input-blk">
										   <input name="swiftcode" class="form-control" id="swiftcode" placeholder="Swift Code" value="<?php echo $swiftcode;?>" type="text"> 
										   <span class="error FormFieldMessageError2"> Swift Code field is required.</span>
										   </div>
										   </div>
									<div class="bank col-md-6 form-group"><div class="label-field"><?php echo $this->lang->line('Routing Number')?></div>
										   <div class="field-input-blk">
										   <input name="routingnumber" class="form-control" id="routingnumber" placeholder="Routing Number" value="<?php echo $routingnumber;?>" type="text"> 
										   <span class="error FormFieldMessageError3"> Routing Number field is required.</span>
										   <span class="error" id="route_number"> </span>
									</div>
										   </div>
								</div>
                                <div class="row">
                                <div class="bank col-md-6 form-group"><div class="label-field"><?php echo $this->lang->line('Account Number')?></div>
										   <div class="field-input-blk">
										   <input name="accountnumber" class="form-control" id="accountnumber" placeholder="Account Number"  value="<?php echo $accountnumber;?>" type="text"> 
										   <span class="error FormFieldMessageError4"> Account Number field is required.</span>
										    <span class="error" id="account_number"> </span>
											</div>
										   </div>
                                           </div>
										   </div>
                            
                              <div class="col-md-12 form-group blue-btn-container">
                              
                              <button type="button" id="creditcard_btn" class="btn btn-primary"><?php echo $this->lang->line('Save Changes')?></button>
                              <div class="gallery_update_loader" >
								<div class="lds-default">
									<div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
									</div>
								</div>
                          </div>
                          </div>
                         
                         
                      </form>
					 
                        </div>						
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>

                <?php  
                    $noMatch = $this->session->flashdata('nomatch');
                    if($noMatch)
                    {
                ?>
                <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('nomatch'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>
<script type="text/javascript">
   $(document).ready(function(){
	   $('#fb-share').click(function(event){		   
		   var app_id= 336551780314990, 
		   link = $(this).data( "url" );
	   window.open('fb-messenger://share?link=' + encodeURIComponent(link) + '&app_id=' + encodeURIComponent(app_id));
	   })
	    $(".HolderFormFieldMessage").css("display", "none");
		$(".FormFieldMessageError1").css("display", "none");
		$(".FormFieldMessageError2").css("display", "none");
		$(".FormFieldMessageError3").css("display", "none");
		$(".FormFieldMessageError4").css("display", "none");
      $('#creditcard_btn').click(function(event){
        event.preventDefault();
        var bankname = $("#bankname").val();
		var swiftcode = $("#swiftcode").val();
		var routingnumber = $("#routingnumber").val();
		var accountnumber = $("#accountnumber").val();
		var holdername = $("#holder_name").val();
		
		 if($("#holder_name").val()=='')
         {
            $("#holder_name").focus();
            $("#holder_name").css("border","1px solid red");
            $(".HolderFormFieldMessage").css("display", "block");
            return false;    
         }
         else
         {
            $("#holder_name").css("border","");
            $(".HolderFormFieldMessage").css("display", "none");
         }
        
        if($("#bankname").val()=='')
         {
            $("#bankname").focus();
            $("#bankname").css("border","1px solid red");
            $(".FormFieldMessageError1").css("display", "block");
            return false;    
         }
         else
         {
            $("#bankname").css("border","");
            $(".FormFieldMessageError1").css("display", "none");
         }
		 
		 if($("#swiftcode").val()=='')
         {
            $("#swiftcode").focus();
            $("#swiftcode").css("border","1px solid red");
            $(".FormFieldMessageError2").css("display", "block");
            return false;    
         }
         else
         {
            $("#swiftcode").css("border","");
            $(".FormFieldMessageError2").css("display", "none");
         }
		 
		 if($("#routingnumber").val()=='')
         {
            $("#routingnumber").focus();
            $("#routingnumber").css("border","1px solid red");
            $(".FormFieldMessageError3").css("display", "block");
            return false;    
         }
         else
         {
            $("#routingnumber").css("border","");
            $(".FormFieldMessageError3").css("display", "none");
         }
		 
		if($("#accountnumber").val()==''){
            $("#accountnumber").focus();
            $("#accountnumber").css("border","1px solid red");
            $(".FormFieldMessageError4").css("display", "block");
            return false;    
         }else{
            $("#accountnumber").css("border","");
            $(".FormFieldMessageError4").css("display", "none");
         }     
        // $(".gallery_update_loader").css("display", "block");
         $.ajax({
                type  : 'POST',
                url   : "<?php echo base_url('user/update_bank_detail') ?>",
                data  : new FormData( $("#basic_info_frm")[0] ),
                contentType : false,
                processData : false,
                success: function(data) 
                {
                  $(".gallery_update_loader").css("display", "none");
                  if(data=="1" || data=="2")
                  {
                    $('html, body').animate({scrollTop : 0},100);
                    $('.successmess').show();
                    $(".successmess").delay(3000).fadeOut("slow");
                  }
                  else
                  {
                    alert("Something Went Wrong");
                  }
                  
                }
            });
			
		

      });
   });
    $('input[name="routingnumber"]').keyup(function(e)
		{
			if (/\D/g.test(this.value))
		{// Filter non-digits from input value.
	        e.preventDefault();
			this.value = this.value.replace(/\D/g, '');
			$("#route_number").show();
			$("#routingnumber").focus();
			$("#routingnumber").css("border","1px solid red");
			$("#route_number").text("Allowed Numbers only").css("color","#c71825");
			
			return false;
		}
		else
		{
			$("#routingnumber").css("border","");
			$("#route_number").css("display", "none");
		
		}	
	});
	$('input[name="accountnumber"]').keyup(function(e)
		{	
			if (/\D/g.test(this.value))
		{// Filter non-digits from input value.
	         e.preventDefault();
			this.value = this.value.replace(/\D/g, '');
			$("#accountnumber").focus();
			$("#accountnumber").css("border","1px solid red");
			$("#account_number").text("Allowed Numbers only").css("color","#c71825"); 
		return false;
		}
		else
		{
			$("#accountnumber").css("border","");
			$("#account_number").css("display", "none");
		
		}	
	});
	/*Bank Name and Account Holder Character Validation   */
	$('input[name="bankname"]').bind('keydown', function(event) {
	var key = event.which;
  if (key >=48 && key <= 57) {
	  $("#bankname").focus();
		$("#bankname").css("border","1px solid red");
		$("#bank_name").text("Numbers Not Allowed").css("color","#c71825"); 
    event.preventDefault();
	return false;
  }
  else
  {
	 $("#bankname").css("border","");
	$("#bank_name").css("display", "none"); 
  }  
});
$('input[name="holder_name"]').bind('keydown', function(event) {
	var key = event.which;
  if (key >=48 && key <= 57) {
	  $("#holder_name").focus();
		$("#holder_name").css("border","1px solid red");
		$("#acc_holder").text("Numbers Not Allowed").css("color","#c71825"); 
    event.preventDefault();
	return false;
  }
  else
  {
	 $("#holder_name").css("border","");
	$("#acc_holder").css("display", "none"); 
  }  
});
	
   </script>