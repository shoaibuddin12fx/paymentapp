<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> User Management
        <small>Add / Edit User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter User Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    <form role="form" id="addUser" method="post" role="form" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Full Name</label>
                                        <input type="text" class="form-control required" value="<?php echo set_value('fname'); ?>" id="fname" name="fname" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email address</label>
                                        <input type="text" class="form-control required email" id="email" value="<?php echo set_value('email'); ?>" name="email" maxlength="128">
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" class="form-control required" id="username" value="<?php echo set_value('username'); ?>" name="username" maxlength="128">
										<span id="emailmsg" class="error join_error"></span>
										<input type="hidden" id="usernamevalid" name="usernamevalid" required="required" value = "<?php  if(set_value('usernamevalid')){echo set_value('emailvalid');  }?>"/>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number</label>
                                        <input type="text" class="form-control required digits" id="mobile" value="<?php echo set_value('mobile'); ?>" name="mobile" maxlength="10">
                                    </div>
                                </div>   
                            </div>
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="profilePic">Profile Image</label>
                                        <input type="file" class="form-control required" id="profile_pic" value="<?php echo set_value('profile_pic'); ?>" name="profile_pic"/>
										<input type="hidden" id="public-photo" name="public_file_upload">
                                    </div>
                                    
                                </div>
								 <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="profilePic">ID Proof</label>
                                        <input type="file" class="form-control required" id="proof_id" value="<?php echo set_value('profile_pic'); ?>" name="proof_id"/>
										<input type="hidden" id="public-photo" name="public_file_upload">
                                    </div>
                                    
                                </div>
								<div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="state">State</label>
                                        <input type="text" class="form-control required" value="<?php echo set_value('state'); ?>" id="state" name="state" maxlength="128">
                                    </div>
                                    
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="city">City</label>
                                        <input type="text" class="form-control required " id="city" value="<?php echo set_value('city'); ?>" name="city" maxlength="128">
                                    </div>
                                </div>
                                
                            </div>
							<div class="row">
                                
                                
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="country">Country</label>
                                        <input type="text" class="form-control required " id="country" value="<?php echo set_value('country'); ?>" name="country" maxlength="128">
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="zip">Zip Code</label>
                                        <input type="text" class="form-control required " id="zipCode" value="<?php echo set_value('zipCode'); ?>" name="zipCode" maxlength="25">
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <input type="text" class="form-control required " id="address" value="<?php echo set_value('address'); ?>" name="address" maxlength="128">
                                    </div>
                                </div>
								
                            </div>
						
                        </div><!-- /.box-body -->
    
                        <div class="box-footer action_box">
                            <input type="submit" class="btn btn-primary" value="Submit" id="formsubmit"/>
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
			
     $("#username").keyup(function() {	
	 
    var usernameget = $("#username").val();
       var formData = "username="+usernameget;
        if (usernameget=="") {
			$('#emailmsg').hide();
        } else {
        $.ajax({
                type        : 'POST', // 
                url         : '<?php echo base_url("checkUsernameExists"); ?>', 
                data        : formData, 
                dataType    : 'html', 
                success: function(data)
                {                
                  if(data ==1)
                  {
                   $('#emailmsg').html("<font style='color:green'></font>");
                   $('#usernamevalid').val("1");
				   $('#emailmsg').hide();
                  } else if(data ==0)
                  {
                    $('#emailmsg').html("Username already taken");
					$('#emailmsg').show();
                    $('#usernamevalid').val("0"); 
                  }
                }   
        });
    }

});

$("#addUser").submit(function(){
  var usernamevalid = $("#usernamevalid").val();
  if(usernamevalid == 0)
  {
	 // $("#username").focus()
	  
	  return false;
  }else{
	  $('#addUser').attr('action', '<?php echo base_url("addNewUser") ?>');
  }
});

     });
 </script>