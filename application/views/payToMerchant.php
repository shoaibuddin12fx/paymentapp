<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Payment History
        <!--<small>Add, Edit, Delete</small> -->
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">
					Manage Transactions
					</h3>
                   
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table id="manageTransaction" class="table table-bordered table-striped dataTable " role="grid" aria-describedby="example1_info">
				  <thead>
                    <tr>
					    <th class="price-list-sno">#</th>
                        <th>Transaction ID</th>
                        <th>Amount</th>                      
						<th>Merchant Email</th>
						<th>Merchant Name</th>	                      
                        <th>Payable Amount</th>
						<th>Payment Status</th>
						<th>Paid Amount</th>
						<th>Paid To Merchant TID</th>
						<th>Paid By Merchant At</th>
                        <th>Pay To Merchant</th>
                        
						 
											      					
                    </tr>
					</thead>
					<tbody>
                    <?php
                    if(!empty($manageTransaction))
                    { $i=0;
						 if(count($manageTransaction)):$inc =1;
						 //echo '<pre>';
						 //print_r($manageTransaction); //die;
                        foreach($manageTransaction as $record)
                        { 
                    ?>
					<!--echo $record->amount - ($record->amount * ($getComission->comission_percentage/100)) -->
                    <tr>
					    <td><?php echo $inc; ?></td>
                        <td><?php echo $record->transaction_id ?></td>
                        <td><?php echo $record->amount ?></td>                                       
						<td><?php echo $record->merchantEmail ?></td>
						<td><?php echo $record->merchantName ?></td>                       
						<td><?php echo $record->payableAmount?> </td>						
						 <td><?php if($record->paid_by_admin==1){echo 'Success'; }else echo 'Pending'; ?></td>
						 <td><?php echo $record->paidToMerchantAmount ?></td>
						 <td><?php echo $record->paidByAdminTransactionId ?></td>
						  <td><?php echo $record->paid_by_admin_at ?></td>
                        <td><?php if($record->paid_by_admin==1){echo 'N/A'; }else echo '<button type="submit" class="btn btn-primary pay" value="Pay" data-toggle="modal" data-target="#myModal" data-accHolder= "'.$record->account_holder.'" data-bankName= "'.$record->bankname.'" data-swiftCode= "'.$record->swiftcode.'" data-rNumber= "'.$record->routingnumber.'" data-accNumber= "'.$record->accountnumber.'" data-clientid= "'.$record->client_id.'" data-payableamount= "'.$record->payableAmount.'" data-ai_id= "'.$record->id.'">Pay</button>'; ?>  </td>
						
                    </tr>
                    <?php
					$i++;
					$inc++;
					// echo "<pre>";
					// print_r($manageTransaction);
                        }endif;
                    }
                    ?>
					</tbody>
                  </table>
                  
                </div><!-- /.box-body -->
               
              </div><!-- /.box -->
            </div>
        </div>
    </section>
	<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pay To Merchant</h4>
        </div>
        <div class="modal-body">
		<div class="row">
		 <div class="col-xs-6">
          <p>Bank Details:-</p>
		 
		  <table class="table-bordered" align="center">
		 <tr><td><label>Account Holder Name:</label></td><td><p id="accHolder"></p></td></tr>
		 <tr><td><label>Bank Name:</label></td><td> <p id="bankName"></p></td></tr>
		 <tr><td><label>Swift Code:</label> </td><td><p id="swiftCode"></p></td></tr>
		 <tr><td><label>Routing Number:</label> </td><td><p id="rNumber"></p></td></tr>
		 <tr><td><label>Account Number:</label></td><td><p id="accNumber"></p></td></tr>
		 </table>
        </div>
		
		
		<div class="col-xs-6">
		<span class="help-block" style="display:none">*All fields are mandatory</span>
		<form id="payment-form">
		<div class="form-group">
		 <label>Payable Amount</label>
		 <input type="number" id="amount" class="form-control"   name="amount" readonly/>
		 <!--<input type="hidden" id="user-id" class="form-control"  name="user-id" /> -->
		 <input type="hidden" id="ai_id" class="form-control"  name="ai_id" />
		 <label>Transaction ID</label>
		 <input type="text" id="transaction_Id" class="form-control" name="transaction_Id" required/>
		 
		 
        </div>
		<div class="form-group">
		<label>Date</label>
                <div class='input-group date'>
				  <input type="text"  name="trans_date" id="trans_date" placeholder="mm-dd-yy" name="trans_date" readonly>
                    <!--<input type='date' id="trans_date" format="mm-dd-yyyy" placeholder="mm-dd-yyyy" class="form-control" name="trans_date" required /> -->
                </div>
            </div>
			 <button type="submit" id="pay2" class="btn btn-primary pay2">Pay</button>
   </form>
		</div>	
</div>		
		
		</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script src="<?php echo base_url('assets/js/jquery.timepicker.js');?>"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.timepicker.css');?>" />
<script>
$(document).ready(function(){
	$( "#trans_date" ).datepicker({
  dateFormat: 'mm-dd-yy',//check change
  changeMonth: true,
  changeYear: true
});	
  $(".pay").click(function(){ 
  $("#accHolder").text($(this).data('accholder'));
  $("#bankName").text($(this).data('bankname'));
  $("#swiftCode").text($(this).data('swiftcode'));
  $("#rNumber").text($(this).data('rnumber'));
  $("#accNumber").text($(this).data('accnumber'));
  $("#amount").val($(this).data('payableamount'));
  $("#user-id").val($(this).data('clientid'));
  $("#ai_id").val($(this).data('ai_id'));
    //alert($(this).data('accholder'));
  });
  $(".pay2").click(function(){
var trans_id = $('#transaction_Id').val();
var trans_date = $('#trans_date').val();
if(trans_id==""|| trans_date== "")
{
    $('.help-block').show();
	return false; 
}else{
	$('.help-block').hide();
  
}
  $.ajax({
                type  : 'POST',
                url   : "<?php echo base_url('payByAdmin') ?>",
                data  : new FormData( $("#payment-form")[0] ),
                contentType : false,
                processData : false,
                success: function(data) 
                {
                  $(".gallery_update_loader").css("display", "none");
                  if(data==1)
                  {
					 alert("Successfully updated") ;
					 location.reload();
                    // $('html, body').animate({scrollTop : 0},100);
                    // $('.successmess').show();
                    // $(".successmess").delay(3000).fadeOut("slow");
                  }
                  else
                  {
                    alert("Plz!, try again later");
                  }
                  
                }
            });
  });
});
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js" charset="utf-8"></script>

  <script>
  $(function(){
    $("#manageTransaction").dataTable();
  })
  </script>  
