<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $pageTitle; ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- FontAwesome 4.3.0 -->
    <link href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>assets/dist/css/backend.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bower_components/datatables.net/css/jquery.dataTables.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <style>
    	.error{
    		color:red;
    		font-weight: normal;
    	}
    </style>
    <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript">
        var baseURL = "<?php echo base_url(); ?>";
    </script>
    <script>
var a2a_config = a2a_config || {};
a2a_config.overlays = a2a_config.overlays || [];
a2a_config.overlays.push({
    services: ['pinterest', 'facebook', 'houzz', 'tumblr','twitter','whatsapp'],
    size: '50',
    style: 'horizontal',
    position: 'top center'
});
</script>
<script async src="https://static.addtoany.com/menu/page.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/sharer.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      
      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>Cash</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CircleCash</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
			<?php

            if($role)
             { ?>
					<li class="dropdown tasks-menu">
					<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true" style="cursor: pointer;"> <span><?php if($this->session->userdata('lang')=='french'){echo 'Français';}elseif($this->session->userdata('lang')=='arabic'){echo 'عربى';}else{echo 'English';}?> </a>
                        <ul class="dropdown-menu">
                        	 <li class="header"><a class="" href="<?php echo base_url('/user/french');?>" class="padingsize"> <span>Français </a></li>
                             <li class="header"><a class="" href="<?php echo base_url('/user/english');?>" class="padingsize"> <span>English </a></li>
                             <li class="header"><a class="" href="<?php echo base_url('/user/arabic');?>" class="padingsize"> <span>عربى</a></li>
                          
						  </ul>
                    </li>
			 <?php }?>
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                  <i class="fa fa-history"></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="header"> Last Login : <i class="fa fa-clock-o"></i> <?= empty($last_login) ? "First Time Login" : $last_login; ?></li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url(); ?>uploads/profile-pics/<?php $profile_pic ='';
                  echo $profile_pic;?>" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?php echo $name; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    
                    <img src="<?php echo base_url(); ?>uploads/profile-pics/<?php echo $profile_pic;?>" class="img-circle" alt="User Image" />
                    <p>
                      <?php echo $name; ?>
                      <small><?php echo $role_text=''; ?></small>
                    </p>
                    
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url(); ?>profile" class="btn btn-warning btn-flat"><i class="fa fa-user-circle"></i><?php echo $this->lang->line('Profile')?></a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i><?php echo $this->lang->line('Sign Out')?></a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header"><?php echo $this->lang->line('Main Navigation')?></li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>dashboard">
                <i class="fa fa-dashboard"></i> <span><?php echo $this->lang->line('Dashboard')?></span></i>
              </a>
            </li>
            <?php
            if($role )
            {
            ?>
            <li class="treeview collapsible">
              <a href="#" >
                <i class="fa fa-thumb-tack"></i>
                <span><?php echo $this->lang->line('Payment History')?></span>
              </a>
			    <ul class="treeview-menu">
            <!--<li><a href="<?php //echo base_url('Smnadmin/current_bids') ?>"><i class="fa fa-fw fa-user"></i>Today Won Bid</a></li>-->
             <li class=""><a href="<?php echo base_url('paymentHistory') ?>/<?php echo base64_encode(base64_encode(base64_encode(base64_encode(1))))?>"><i class="fa fa-fw fa-user"></i><?php echo $this->lang->line('Sales Panel')?></a></li>
			 <li><a href="<?php echo base_url('paymentHistory') ?>/<?php echo base64_encode(base64_encode(base64_encode(base64_encode(2))))?>"><i class="fa fa-fw fa-user"></i><?php echo $this->lang->line('Payment From Admin')?></a></li>
			
          </ul>
            </li>
            <?php
            }
            if($role )
            {
            ?>
            <li class="treeview collapsible">
              <a href="#" >
                <i class="fa fa-thumb-tack"></i>
                <span>Users</span>
              </a>
			    <ul class="treeview-menu">
            <li><a href="<?php echo base_url('Smnadmin/current_bids') ?>"><i class="fa fa-fw fa-user"></i>Today Won Bid</a></li>
             <li class=""><a href="<?php echo base_url('merchantListing') ?>/<?php echo base64_encode(base64_encode(base64_encode(base64_encode(1))))?>"><i class="fa fa-fw fa-user"></i>Merchants</a></li>
			 <li><a href="<?php echo base_url('customerListing') ?>/<?php echo base64_encode(base64_encode(base64_encode(base64_encode(2))))?>"><i class="fa fa-fw fa-user"></i>Customers</a></li>
			
          </ul>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url('manageTransaction') ?>" >
                <i class="fa fa-files-o"></i>
                <span>Manage Transactions</span>
              </a>
            </li>
			<li class="treeview">
              <a href="<?php echo base_url('manageComission') ?>" >
                <i class="fa fa-files-o"></i>
                <span>Manage Comission</span>
              </a>
            </li>
			<li class="treeview">
              <a href="<?php echo base_url('payToMerchant') ?>" >
                <i class="fa fa-files-o"></i>
                <span>Pay To Merchant</span>
              </a>
            </li>
			<li class="treeview">
              <a href="<?php echo base_url('overviewDailyPayment') ?>" >
                <i class="fa fa-files-o"></i>
                <span>Daily Payment Overview</span>
              </a>
            </li>
            <?php
            }
            ?>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>