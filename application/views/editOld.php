<?php
$userId = $userInfo->userId;
$name = $userInfo->name;
$email = $userInfo->email;
$mobile = $userInfo->mobile;
$roleId = $userInfo->roleId;
$qrFileName = $userInfo->qr_file_name;
$address = $userInfo->address;
$city = $userInfo->city;
$zipCode = $userInfo->zipCode;
$country = $userInfo->country;
$state = $userInfo->state;
$profile_pic = $userInfo->profile_image;
$proof_id = $userInfo->proof_id;
$username = $userInfo->username;

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> User Management
        <small>Add / Edit User</small>
      </h1>
    </section>
    <?php //print_r($userInfo); ?>
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                <div class="box box-primary">
                    <div class="box-header">
					 <img class="profile-user-img img-responsive img-circle" src="<?php if(empty($profile_pic)){echo base_url(); ?>assets/dist/img/<?php echo "avatar.png" ; }else {echo base_url(); ?>uploads/profile-pics/<?php echo $profile_pic ; }?>" alt="User profile picture"/><br>
					 <center><h2 class="box-title">Username:- </h2><b><?php echo "&nbsp  ".$username;?></b></center><br>
                        <h3 class="box-title">Enter User Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>editUser" method="post" id="editUser" role="form" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Full Name</label>
                                        <input type="text" class="form-control" id="fname" placeholder="Full Name" name="fname" value="<?php echo $name; ?>" maxlength="128">
                                        <input type="hidden" value="<?php echo $userId; ?>" name="userId" id="userId" />    
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email address</label>
                                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="<?php echo $email; ?>" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password" placeholder="Password" name="password" maxlength="20">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword">Confirm Password</label>
                                        <input type="password" class="form-control" id="cpassword" placeholder="Confirm Password" name="cpassword" maxlength="20">
                                    </div>
                                </div>
                            </div>
							 <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="profilePic">Image</label>
                                        <input type="file" class="form-control " id="profile_pic" value="<?php  ?>" name="profile_pic"/>
										<input type="hidden" value="<?php echo $profile_pic; ?>" name="pro_pic"/>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="country">Country</label>
                                        <input type="text" class="form-control required " id="country" value="<?php echo $country; ?>" name="country" maxlength="128">
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="state">State</label>
                                        <input type="text" class="form-control required" value="<?php echo $state; ?>" id="state" name="state" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="city">City</label>
                                        <input type="text" class="form-control required " id="city" value="<?php echo $city ; ?>" name="city" maxlength="128">
                                    </div>
                                </div>
							</div>
								<div class="row">
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <input type="text" class="form-control required " id="address" value="<?php echo $address; ?>" name="address" maxlength="128">
                                    </div>
									<div class="form-group">
                                        <label for="zip">Zip Code</label>
                                        <input type="text" class="form-control required " id="zipCode" value="<?php echo $zipCode; ?>" name="zipCode" maxlength="128">
                                    </div>
                                </div>
								 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number</label>
                                        <input type="text" class="form-control" id="mobile" placeholder="Mobile Number" name="mobile" value="<?php echo $mobile; ?>" maxlength="10">
                                    </div>
                                </div>
                            </div>
                            <div class="row qr_id_main">			
                                   <div class="col-md-6">
                                    <div class="form-group">
										 <img src="<?php echo base_url('uploads/qrcodes/').$qrFileName ?>" alt=""/>
                                        <label for="mobile">QR Code</label>                                       
										<input type="hidden" name="qr_file_name" value="<?php echo $qrFileName ?>">
                                    </div>
								  </div>								
									<div class="col-md-6">
                                    <div class="form-group">
										<?php $idProof_fileExtensions = ['jpeg','png','jpg','pdf'];
										$idProof_tmp = explode('.', $proof_id);
										$idProof_fileExtension = end($idProof_tmp);
										?>                                        
										<?php if(empty($proof_id)){
											echo "<h4><span class='label label-warning'>Id-Proof Not Availble</span></h4>";
										}
										if($idProof_fileExtension=='pdf'){
                                        echo '<a href="'.base_url('uploads/id-proofs/').$proof_id. '" target="_blank" alt=""/>Click to View</a>';
										}else {
										echo '<a href="'.base_url('uploads/id-proofs/').$proof_id.'" target="_blank" alt=""/><img src="'.base_url('uploads/id-proofs/').$proof_id.'" height="296" width="296" alt=""/></a>';
										}?>
										<label for="mobile">ID Proof</label>
                                    </div>
                                </div>	

                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer action_box">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>