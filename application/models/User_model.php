<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : User_model (User Model)
 * User model class to get to handle user related data 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class User_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function userListingCount($searchText = '')
    {
		if($this->session->userdata('userlisting_type')==1)
		{
			$this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, BaseTbl.createdDtm, Role.role');
			$this->db->from('tbl_users as BaseTbl');
			$this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
			if(!empty($searchText)) {
				$likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
								OR  BaseTbl.name  LIKE '%".$searchText."%'
								OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
				$this->db->where($likeCriteria);
			}
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.roleId', 2);
			$query = $this->db->get();
        }else{
			$this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, BaseTbl.createdDtm, Role.role');
			$this->db->from('tbl_users as BaseTbl');
			$this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
			if(!empty($searchText)) {
				$likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
								OR  BaseTbl.name  LIKE '%".$searchText."%'
								OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
				$this->db->where($likeCriteria);
			}
			$this->db->where('BaseTbl.isDeleted', 0);
			$this->db->where('BaseTbl.roleId', 3);
			$query = $this->db->get();
		}
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function userListing($searchText = '', $page, $segment, $type='')
    {
		
		if($this->session->userdata('userlisting_type')==1)
		{
			$this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, BaseTbl.createdDtm, Role.role, BaseTbl.isDeleted as isDeactivated');
			$this->db->from('tbl_users as BaseTbl');
			// $this->db->where('Role.roleId',2);
			$this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
			
			if(!empty($searchText)) {
				$likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
								OR  BaseTbl.name  LIKE '%".$searchText."%'
								OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
				$this->db->where($likeCriteria);
			}
			
			$this->db->where('BaseTbl.roleId', 2);
			$this->db->order_by('BaseTbl.userId', 'DESC');
			$this->db->limit($page, $segment);
			$query = $this->db->get();      
			$result = $query->result();  
		}else{
			
			$this->db->select('BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, BaseTbl.createdDtm, Role.role,BaseTbl.isDeleted as isDeactivated');
			$this->db->from('tbl_users as BaseTbl');
			 //$this->db->where('Role.roleId',3);
			$this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
			
			if(!empty($searchText)) {
				$likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
								OR  BaseTbl.name  LIKE '%".$searchText."%'
								OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
				$this->db->where($likeCriteria);
			}
			
			$this->db->where('BaseTbl.roleId', 3);
			$this->db->order_by('BaseTbl.userId', 'DESC');
			$this->db->limit($page, $segment);
			$query = $this->db->get();      
			$result = $query->result();  
		}			
        return $result;
    }
    
    /**
     * This function is used to get the user roles information
     * @return array $result : This is result of the query
     */
    function getUserRoles()
    {
        $this->db->select('roleId, role');
        $this->db->from('tbl_roles');
        $this->db->where('roleId !=', 1);
        $query = $this->db->get();
        
        return $query->result();
    }

    /**
     * This function is used to check whether email id is already exist or not
     * @param {string} $email : This is email id
     * @param {number} $userId : This is user id
     * @return {mixed} $result : This is searched result
     */
    function checkEmailExists($email, $userId = 0)
    {
        $this->db->select("email");
        $this->db->from("tbl_users");
        $this->db->where("email", $email);   
        if($userId != 0){
            $this->db->where("userId !=", $userId);
        }
        $query = $this->db->get();

        return $query->result();
    }
    
    /**
     * This function is used to check whether username is already exist or not
     * @param {string} $username : This is email id
     * @param {number} $userId : This is user id
     * @return {mixed} $result : This is searched result
     */
    function checkUsernameExists($username, $userId = 0)
    {
        $this->db->select("username");
        $this->db->from("tbl_users");
        $this->db->where("username", $username);   
        if($userId != 0){
            $this->db->where("userId !=", $userId);
        }
        $query = $this->db->get();

        return $query->result();
    }
	
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewUser($userInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_users', $userInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfo($userId)
    {
        $this->db->select('userId, proof_id, name, zipCode, email, mobile, username, roleId, qr_file_name, profile_image, address, state, city, country');
        $this->db->from('tbl_users');       
		$this->db->where('roleId !=', 1);
        $this->db->where('userId', $userId);
        $query = $this->db->get();
        
        return $query->row();
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editUser($userInfo, $userId)
    {
        $this->db->where('userId', $userId);
        $this->db->update('tbl_users', $userInfo);
        
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser($userId)
    {
        $this->db->where('userId', $userId);
        $result = $this->db->delete('tbl_users');
        
        return $result;
    }
	/**
     * This function is used to Activate/Deactivate the user 
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
	function deactivateActivateUser($userId, $userInfo)
    {
        $this->db->where('userId', $userId);
        $this->db->update('tbl_users', $userInfo);
        
        return $this->db->affected_rows();
    }


    /**
     * This function is used to match users password for change password
     * @param number $userId : This is user id
     */
    function matchOldPassword($userId, $oldPassword)
    {
        $this->db->select('userId, password');
        $this->db->where('userId', $userId);        
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('tbl_users');
        
        $user = $query->result();

        if(!empty($user)){
            if(verifyHashedPassword($oldPassword, $user[0]->password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }
    
    /**
     * This function is used to change users password
     * @param number $userId : This is user id
     * @param array $userInfo : This is user updation info
     */
    function changePassword($userId, $userInfo)
    {
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', $userInfo);
        
        return $this->db->affected_rows();
    }


    /**
     * This function is used to get user login history
     * @param number $userId : This is user id
     */
    function loginHistoryCount($userId, $searchText, $fromDate, $toDate)
    {
        $this->db->select('BaseTbl.userId, BaseTbl.sessionData, BaseTbl.machineIp, BaseTbl.userAgent, BaseTbl.agentString, BaseTbl.platform, BaseTbl.createdDtm');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.sessionData LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        if(!empty($fromDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) >= '".date('Y-m-d', strtotime($fromDate))."'";
            $this->db->where($likeCriteria);
        }
        if(!empty($toDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) <= '".date('Y-m-d', strtotime($toDate))."'";
            $this->db->where($likeCriteria);
        }
        if($userId >= 1){
            $this->db->where('BaseTbl.userId', $userId);
        }
        $this->db->from('tbl_last_login as BaseTbl');
        $query = $this->db->get();
        
        return $query->num_rows();
    }

    /**
     * This function is used to get user login history
     * @param number $userId : This is user id
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function loginHistory($userId, $searchText, $fromDate, $toDate, $page, $segment)
    {
        $this->db->select('BaseTbl.userId, BaseTbl.sessionData, BaseTbl.machineIp, BaseTbl.userAgent, BaseTbl.agentString, BaseTbl.platform, BaseTbl.createdDtm');
        $this->db->from('tbl_last_login as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.sessionData  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        if(!empty($fromDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) >= '".date('Y-m-d', strtotime($fromDate))."'";
            $this->db->where($likeCriteria);
        }
        if(!empty($toDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) <= '".date('Y-m-d', strtotime($toDate))."'";
            $this->db->where($likeCriteria);
        }
        if($userId >= 1){
            $this->db->where('BaseTbl.userId', $userId);
        }
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfoById($userId)
    {
        $this->db->select('userId, name, email, mobile, roleId');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
        $this->db->where('userId', $userId);
        $query = $this->db->get();
        
        return $query->row();
    }

    /**
     * This function used to get user information by id with role
     * @param number $userId : This is user id
     * @return aray $result : This is user information
     */
    function getUserInfoWithRole($userId)
    {
        $this->db->select('BaseTbl.userId, BaseTbl.qr_file_name, BaseTbl.email, BaseTbl.username, BaseTbl.address,BaseTbl.country, BaseTbl.city,BaseTbl.zipCode,BaseTbl.state, BaseTbl.profile_image, BaseTbl.name, BaseTbl.mobile, BaseTbl.roleId, Roles.role, BankTbl.user_id, BankTbl.account_holder, BankTbl.bankname, BankTbl.swiftcode, BankTbl.routingnumber, BankTbl.accountnumber');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Roles','Roles.roleId = BaseTbl.roleId');
		$this->db->join('tbl_bank_details BankTbl', 'BankTbl.user_id=BaseTbl.userId', 'left');
        //$this->db->where('BaseTbl.userId', $userId);
        $this->db->where("(BaseTbl.userId = '$userId' OR BaseTbl.username = '$userId' OR BaseTbl.email = '$userId')");
       // $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        
        return $query->row();
    }
	function getPaymentHistoryByType($type,$userId)
    {
		if($type==1)
		{
			$this->db->select('TransTbl.amount,TransTbl.transaction_id,TransTbl.customer_id,TransTbl.amount,TransTbl.client_id,TransTbl.*,BaseTblClient.name as merchantName,BaseTblClient.email as merchantEmail,BaseTblCustomer.name as customerName,BaseTblCustomer.email as customerEmail');
			$this->db->from('tbl_transaction_sales TransTbl');
		    $this->db->join('tbl_users BaseTblClient', 'BaseTblClient.userId=TransTbl.client_id');
		    $this->db->join('tbl_users BaseTblCustomer', 'BaseTblCustomer.userId=TransTbl.customer_id', 'left');
			$this->db->where('client_id',$userId);
		    return $query = $this->db->get()->result();
			
		}else{
			$this->db->select('TransTbl.amount,TransTbl.transaction_id,TransTbl.customer_id,TransTbl.amount,TransTbl.client_id,TransTbl.*,BaseTblClient.name as merchantName,BaseTblClient.email as merchantEmail,BaseTblCustomer.name as customerName,BaseTblCustomer.email as customerEmail');
			$this->db->from('tbl_transaction_sales TransTbl');
		    $this->db->join('tbl_users BaseTblClient', 'BaseTblClient.userId=TransTbl.client_id');
		    $this->db->join('tbl_users BaseTblCustomer', 'BaseTblCustomer.userId=TransTbl.customer_id', 'left');
			$this->db->where("paid_by_admin",1);
			$this->db->where('client_id',$userId);
		    return $query = $this->db->get()->result();
			
		}       
        
    }
	function getAllTransaction()
	{
		$this->db->select('TransTbl.amount,TransTbl.id,TransTbl.payableAmount,TransTbl.paidByAdminTransactionId,TransTbl.transaction_id,TransTbl.payment_status,TransTbl.customer_id,TransTbl.amount,TransTbl.client_id,TransTbl.payment_date_time,TransTbl.payableAmount,TransTbl.paid_by_admin,TransTbl.paid_by_admin_at,TransTbl.paidToMerchantAmount,BaseTblClient.name as merchantName,BaseTblClient.email as merchantEmail,BaseTblCustomer.name as customerName,BaseTblCustomer.email as customerEmail, BaseTblCustomerBank.user_id,BaseTblCustomerBank.account_holder,BaseTblCustomerBank.bankname,BaseTblCustomerBank.swiftcode,BaseTblCustomerBank.routingnumber,BaseTblCustomerBank.accountnumber');
			$this->db->from('tbl_transaction_sales TransTbl');
		    $this->db->join('tbl_users BaseTblClient', 'BaseTblClient.userId=TransTbl.client_id');
		    $this->db->join('tbl_users BaseTblCustomer', 'BaseTblCustomer.userId=TransTbl.customer_id', 'left');
		    $this->db->join('tbl_bank_details BaseTblCustomerBank', 'BaseTblCustomerBank.user_id=TransTbl.client_id', 'left');
		return $query = $this->db->get()->result();

	}
	function getComissionpercentage()
	{
		$this->db->select('*');
		$this->db->from('tbl_manage_comission');
		return $query1 = $this->db->get()->row();
	}
	function UpdateCommisionpercentage($type,$comissionInfo)
	{
		if($type==1 && $comissionInfo)
		{
			$this->db->where('id', $comissionInfo['id']);
            $this->db->update('tbl_manage_comission', $comissionInfo);
            return $this->db->affected_rows();
		}
		
	}
	function getBankDetails($userId)
	{
		return $result = $this->db->select('*')
		         ->from('tbl_bank_details')
				 ->where("user_id",$userId)
				 ->get()->row();
	}
	
	function getPaymentHistoryCustomer($userId)
	{
		$this->db->select('TransTbl.amount,TransTbl.transaction_id,TransTbl.customer_id,TransTbl.amount,TransTbl.client_id,TransTbl.*,BaseTblClient.name as merchantName,BaseTblClient.email as merchantEmail,BaseTblCustomer.name as customerName,BaseTblCustomer.email as customerEmail');
			$this->db->from('tbl_transaction_sales TransTbl');
			$this->db->where("customer_id",$userId);		
		    $this->db->join('tbl_users BaseTblClient', 'BaseTblClient.userId=TransTbl.client_id');
		    $this->db->join('tbl_users BaseTblCustomer', 'BaseTblCustomer.userId=TransTbl.customer_id', 'left');
			$this->db->order_by("TransTbl.id", "desc");
		return $query = $this->db->get()->result();
	}
	function overviewDailyPayment()
	{
		return $result = $this->db->select('*')
		         ->from('tbl_transaction_sales TransTbl')
				 ->where("paid_by_admin",1)
				 ->join('tbl_users BaseTbl', 'BaseTbl.userId=TransTbl.client_id', 'left')
				 ->where("paid_by_admin_at",date('m-d-Y'))
				 //->where("DATE_FORMAT(paid_by_admin_at, '%m-%d-%Y')=",date('m-d-Y'))
				 ->get()->result();
	}
	function updateClientAdminPay($userInfo, $aIId)
    {
        $this->db->where('id', $aIId);
        $this->db->update('tbl_transaction_sales', $userInfo);
        $result=$this->db->affected_rows();
        return $result;
    }
	function universalUpdate($userInfo, $userId, $tableName,$whereKey)
    {
        $this->db->where($whereKey, $userId);
        $this->db->update($tableName, $userInfo);
        $result=$this->db->affected_rows();
        return $result;
    }
	function insertAnyData($table,$info)
	{
		$this->db->trans_start();
		$result = $this->db->insert($table,$info);
		$this->db->trans_complete();
		return $result;
	}
	//DATE_FORMAT(createdDate, '%Y-%m-%d')

}

  