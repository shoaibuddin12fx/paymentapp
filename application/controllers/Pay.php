<?php

require_once './vendor/autoload.php';
use Twilio\Rest\Client;
use Twilio\TwiML\VoiceResponse;

class Pay extends CI_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model'); 
        $this->load->model('login_model'); 
		$this->load->library('email');
        $this->load->helper('array');
		$lang= ($this->session->userdata('lang')) ?
		$this->session->userdata('lang') : 'english';
        $this->lang->load('trans',$lang);
		
    }
// Your Account SID and Auth Token from twilio.com/console

// In production, these should be environment variables. E.g.:
// $auth_token = $_ENV["TWILIO_ACCOUNT_SID"]

// A Twilio number you own with Voice capabilities
// $twilio_number = "+12017205998";
// // Where to make a voice call (your cell phone?)
// $to_number = "+91 96545 94807";

// $client = new Client($account_sid, $auth_token);
// $client->account->calls->create(  
    // $to_number,
    // $twilio_number,
    // array(
        // "url" => "https://handler.twilio.com/twiml/EHd097be72e27b7a041c0931f05587b892"
    // )
// );

function pay(){
	$account_sid = 'ACd97707f0d3d9f2168aa01e93ed2c8885';
$auth_token = '92d91c60c10bc90e13d54d6e414cd5a9';
	$response = new VoiceResponse($account_sid, $auth_token);
	$response->say('Calling Twilio Pay');
	$response->pay(['chargeAmount' => '30.45',
		'action' => 'https://russet-collie-6560.twil.io/pay']);
		

 echo $response;
}
}
?>