<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class User extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->isLoggedIn();   
		$this->load->helper('array');
		$this->load->library('email');      
        $this->load->helper('share');
		$lang= ($this->session->userdata('lang')) ?
		$this->session->userdata('lang') : 'english';
        $this->lang->load('trans',$lang);
    }
	
	public function english(){
		$this->session->unset_userdata('lang');
		$this->session->set_userdata('lang','english');
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		
	}
	public function french(){
		$this->session->unset_userdata('lang');
		$this->session->set_userdata('lang','french');
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		
	}
	public function arabic(){
		$this->session->unset_userdata('lang');
		$this->session->set_userdata('lang','arabic');
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		
	}
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'CodeInsect : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }
    
    /**
     * This function is used to load the Merchant list
     */
    function userListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
			$type =  $this->uri->segment(2);
            if($type ==="VkZaRk9WQlJQVDA9" || $type ==="VkZkak9WQlJQVDA9" )			
            $this->session->set_userdata('userlisting_type',base64_decode(base64_decode(base64_decode(base64_decode($type)))));			
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->userListingCount($searchText);

			$returns = $this->paginationCompress ( $this->uri->segment(1)."/", $count, 10 );
            
            $data['userRecords'] = $this->user_model->userListing($searchText, $returns["page"], $returns["segment"],$this->session->userdata('userlisting_type'));
            
            $this->global['pageTitle'] = 'CodeInsect : User Listing';
            
            $this->loadViews("userListing", $this->global, $data, NULL);
        }
    }
	

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            
            $this->global['pageTitle'] = 'CodeInsect : Add New User';

            $this->loadViews("addNew", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewUser()
    {
		
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
           // $this->form_validation->set_rules('password','Password','required|max_length[20]');
           // $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
           $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
            
           
			$uploadDirectory = "./uploads/profile-pics/";

			$errors = []; // Store all foreseen and unforseen errors here

			$fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
			
			
			 $files = $_FILES;
			 $fileName  = $_FILES['profile_pic']['name']= time().rand(100,1000).$files['profile_pic']['name'];
			 $fileType=   $_FILES['profile_pic']['type']= $files['profile_pic']['type'];
             $fileTmpName=   $_FILES['profile_pic']['tmp_name']= $files['profile_pic']['tmp_name'];
             $fileSize=   $_FILES['profile_pic']['size']= $files['profile_pic']['size'];
			 $tmp = explode('.', $fileName);
             $fileExtension = end($tmp);
             $uploadPath = $uploadDirectory . basename($fileName); 

			if (! in_array($fileExtension,$fileExtensions)) {
				$errors = "This file extension is not allowed. Please upload a JPEG or PNG file";
				$this->session->set_flashdata('error', 'This profile image extension is not allowed. Please upload a JPEG or PNG file');
			}

			if ($fileSize > 2000000) {
				 $errors = "The image file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
				 $this->session->set_flashdata('error', 'The image file is more than 2MB. Sorry, it has to be less than or equal to 2MB');
			}

        
               
         $idProof_uploadDirectory = "./uploads/id-proofs/";


			$idProof_fileExtensions = ['jpeg','png','jpg','pdf']; // Get all the file extensions
			
			
			 $files = $_FILES;
			 $idProof_fileName  = $_FILES['proof_id']['name']= time().rand(100,1000).$files['proof_id']['name'];
			 $idProof_fileType=   $_FILES['proof_id']['type']= $files['proof_id']['type'];
             $idProof_fileTmpName=   $_FILES['proof_id']['tmp_name']= $files['proof_id']['tmp_name'];
             $idProof_fileSize=   $_FILES['proof_id']['size']= $files['proof_id']['size'];
			 $idProof_tmp = explode('.', $idProof_fileName);
             $idProof_fileExtension = end($idProof_tmp);
             $idProof_uploadPath = $idProof_uploadDirectory . basename($idProof_fileName); 

			if (! in_array($idProof_fileExtension,$idProof_fileExtensions)) {
				$errors = "This file extension is not allowed. Please upload a JPEG or PDF file";
				$this->session->set_flashdata('error', 'This id-proof file extension is not allowed. Please upload a JPEG or PDF file');
			}

			if ($idProof_fileSize > 2000000) {
				$errors = "The image file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
				 $this->session->set_flashdata('error', 'The id-proof file is more than 2MB. Sorry, it has to be less than or equal to 2MB');
			}
                if(!empty($errors)){
					$this->session->flashdata('error');
				}else{
					    $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
						$idProof_didUpload = move_uploaded_file($idProof_fileTmpName, $idProof_uploadPath);
						if (!$idProof_didUpload || !$didUpload) {
							$this->session->set_flashdata('error', 'Error in Id-Proof upload, try again');
						}
						$country = $this->security->xss_clean($this->input->post('country')); 
						$state = $this->security->xss_clean($this->input->post('state')); 
						$city = $this->security->xss_clean($this->input->post('city')); 
						$zipCode = $this->security->xss_clean($this->input->post('zipCode')); 
						$address = $this->security->xss_clean($this->input->post('address')); 
						$name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
						$username = strtolower($this->security->xss_clean($this->input->post('username')));
						$email = strtolower($this->security->xss_clean($this->input->post('email')));
						$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
						$pass = array(); //remember to declare $pass as an array
						$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
						for ($i = 0; $i < 8; $i++) {
							$n = rand(0, $alphaLength);
							$pass[] = $alphabet[$n];
						}
						$password = implode($pass);
						$roleId = $this->input->post('role');
						$mobile = $this->security->xss_clean($this->input->post('mobile'));              
						$data['img_url']="";		
						$this->load->library('ciqrcode');
						$userInfo = array('email'=>$email, 'username'=>$username, 'password'=>getHashedPassword($password),'city'=>$city,'zipCode'=>$zipCode,'country'=>$country, 'address'=>$address,'profile_image'=>$fileName,'proof_id'=>$idProof_fileName,'state'=>$state, 'roleId'=>2,'name'=> $name,'mobile'=>$mobile, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));
						
						$this->load->model('user_model');
						$result = $this->user_model->addNewUser($userInfo);
						
						if($result > 0)
						{
							$result = $this->user_model->getUserInfoWithRole($email);
							$qr_image= rand().'.png';
						$params['data'] = $result->userId;
						$params['level'] = 'H';
						$params['size'] = 8;
						$params['savename'] ="./uploads/qrcodes/".$qr_image;
						if($this->ciqrcode->generate($params))
						{
							$data['img_url']=$qr_image;	
							$info= array('qr_file_name'=>$qr_image);
							$this->user_model->universalUpdate($info,$result->userId,'tbl_users','userId');
						}
									 $to = $email; 
									 $subject = "Registration Successfull";
									 $from = 'no-reply@sugarmenow.com';
									 $message = "
									 <html>
									 <head>
									 <title> Hello " . ucfirst($name)."</title>
									 </head>
									 <body>
									 <p>Hello " . ucfirst($name)." ,<br><br>							
									 Welcome to CircleCash<br>
									 Your account has been created. Followings are your Login Credentials:-<br><br>	
											 Email:- " .$email.
											 "<br>Username:- " .$username.
											 "<br>Password:- " .$password.
											 "<br><br>You can use your email or username with password to login.
                                  <br>Download your QR code from attachment.	<br><br>											 
											Thanks <br>
											CircleCash Team
									 </body>
									 </html>
									 ";
									 $headers = "MIME-Version: 1.0" . "\r\n";
									 $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
									 $headers .= 'From: '.$from."\r\n".
										   'Reply-To: '.$from."\r\n" .
							$this->session->set_flashdata('success', 'New User created successfully');
							$parameters = array(
							'to' =>$to,
							//'from' => $email,
							'subject' => $subject,
							'message' => $message
						);
						$this->email->attach(base_url()."/uploads/qrcodes/".$qr_image);
						$this->email->sendMail($parameters);
						}
						else
						{
							$this->session->set_flashdata('error', 'User creation failed');
						}
			}
                
                redirect('addNew');
            
        }
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($userId = NULL)
    {
		$this->session->set_userdata('referred_from', current_url());
        if($this->isAdmin() == TRUE || $userId == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($userId == null)
            {
                redirect('userListing');
            }
            
            $data['roles'] = $this->user_model->getUserRoles();
            $data['userInfo'] = $this->user_model->getUserInfo($userId);      
            $this->global['pageTitle'] = 'PaymentApp : Edit User';
            
            $this->loadViews("editOld", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $userId = $this->input->post('userId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            //$this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password','Password','matches[cpassword]|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');
            //$this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($userId);
            }
            else
            {
					if(($_FILES['profile_pic']['size'])==0)
			{
				$fileName = $this->input->post('pro_pic'); 
			}else{
			$uploadDirectory = "./uploads/profile-pics/";

			$errors = []; // Store all foreseen and unforseen errors here

			$fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
			 $files = $_FILES;
			 $fileName  = $_FILES['profile_pic']['name']= time().rand(100,1000).$files['profile_pic']['name'];
			 $fileType=   $_FILES['profile_pic']['type']= $files['profile_pic']['type'];
             $fileTmpName=   $_FILES['profile_pic']['tmp_name']= $files['profile_pic']['tmp_name'];
             $fileSize=   $_FILES['profile_pic']['size']= $files['profile_pic']['size'];
			 $tmp = explode('.', $fileName);
             $fileExtension = end($tmp);
             $uploadPath = $uploadDirectory . basename($fileName); 

				if (! in_array($fileExtension,$fileExtensions)) {
					$errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
					 $this->session->set_flashdata('error', 'This file extension is not allowed. Please upload a JPEG or PNG file');
				}

				if ($fileSize > 2000000) {
					$errors[] = "The image file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
					 $this->session->set_flashdata('error', 'The image file is more than 2MB. Sorry, it has to be less than or equal to 2MB');
				}

		  if(!empty($errors)){
					$this->session->flashdata('error'); 
				}else{
				   $didUpload = move_uploaded_file($fileTmpName, $uploadPath);                              			   
					if (!$didUpload) {
						$errors[] = "Error in image upload, try again";
						 $this->session->set_flashdata('error', 'Error in image upload, try again');
					} 
					$result = $this->user_model->getUserInfoWithRole($userId);
					unlink("./uploads/profile-pics/".$result->profile_image);	
				}
			}
			if(empty($errors))
			{
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $qrFileName = $this->security->xss_clean($this->input->post('qr_file_name')); 
                $country = $this->security->xss_clean($this->input->post('country')); 
                $state = $this->security->xss_clean($this->input->post('state')); 
                $city = $this->security->xss_clean($this->input->post('city')); 
                $zipCode = $this->security->xss_clean($this->input->post('zipCode')); 
                $address = $this->security->xss_clean($this->input->post('address'));  
                $userInfo = array();
                if(empty($password))
                {
                    $userInfo = array('email'=>$email, 'roleId'=>2, 'name'=>$name,
                                    'mobile'=>$mobile,'profile_image'=>$fileName, 'address'=>$address, 'city'=>$city, 'zipCode'=>$zipCode,'country'=>$country, 'state'=>$state, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
                }
                else
                {
                    $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>2, 'profile_image'=>$fileName, 'address'=>$address, 'city'=>$city,'zipCode'=>$zipCode ,'country'=>$country, 'state'=>$state, 'name'=>ucwords($name), 'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
                }
                
                $result = $this->user_model->editUser($userInfo, $userId);
                
                if($result == true)
                {
					if(!empty($password))
					{
						$to = $email; 
									 $subject = "Password Change Alert CircleCash";
									// $from = 'no-reply@sugarmenow.com';
									 $message = "
									 <html>
									 <head>
									 <title> Hello " . ucfirst($name)."</title>
									 </head>
									 <body>
									 <p>Hello " . ucfirst($name)." ,<br><br>							
									 Your password has been changed.<br><br>	
											Your new password is:- " . $password."<br>
											Email:- " . $email."
											 <br><br>											 
											Thanks <br>
											CircleCash Team
									 </body>
									 </html>
									 ";
									 $headers = "MIME-Version: 1.0" . "\r\n";
									 $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
									 
							//$this->session->set_flashdata('success', 'New User created successfully');
							$parameters = array(
							'to' =>$to,
							//'from' => $email,
							'subject' => $subject,
							'message' => $message
						);
						//$this->email->attach(base_url()."/uploads/qrcodes/".$qr_image);
						$this->email->sendMail($parameters);
					}
                    $this->session->set_flashdata('success', 'User updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User updation failed');
                }
    //redirect($this->session->userdata('referred_from'),refresh);
                redirect($this->session->userdata('referred_from'));
            }else{
				$data['roles'] = $this->user_model->getUserRoles();
            $data['userInfo'] = $this->user_model->getUserInfo($userId);
            
            $this->global['pageTitle'] = 'PaymentApp : Edit User';
            $this->session->flashdata('error');
            $this->loadViews("editOld", $this->global, $data, NULL);
			}
		  }
        }
    }

    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');          
            $result = $this->user_model->deleteUser($userId);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
	
	/**
     * This function is used to Activate/Deactivate the user using userId
     * @return boolean $result : TRUE / FALSE
     */
	function activateDeactivateUser()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId'); 
           $requestType = $this->input->post('request_type'); 
           $email = $this->input->post('email'); 
           $name = $this->input->post('name'); 
			if($requestType==1)
			{
             $userInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
			 $result = $this->user_model->deactivateActivateUser($userId, $userInfo);
			 if($result>0)
				 $result ="deactivated";
			}
            else{
			$userInfo = array('isDeleted'=>0,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            $result = $this->user_model->deactivateActivateUser($userId, $userInfo);
			if($result>0)
				 $result ="activated";
			}          
            if($result == "deactivated") { 
			
			                         $to = $email; 
									 $subject = "Account Alert CircleCash";
									// $from = 'no-reply@sugarmenow.com';
									 $message = "
									 <html>
									 <head>
									 <title> Hello " . ucfirst($name)."</title>
									 </head>
									 <body>
									 <p>Hello " . ucfirst($name)." ,<br><br>							
									 Your account has been deactivated due to some inappropriate behaviour. Contact administrator for resolution.<br><br>	
											 Admin's Email:- admin@circlecash.net
											 <br><br>											 
											Thanks <br>
											CircleCash Team
									 </body>
									 </html>
									 ";
									 $headers = "MIME-Version: 1.0" . "\r\n";
									 $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
									 
							//$this->session->set_flashdata('success', 'New User created successfully');
							$parameters = array(
							'to' =>$to,
							//'from' => $email,
							'subject' => $subject,
							'message' => $message
						);
						//$this->email->attach(base_url()."/uploads/qrcodes/".$qr_image);
						$this->email->sendMail($parameters);
			           echo(json_encode(array('status'=>'deactivated'))); 			
			}elseif($result == "activated"){
				$to = $email; 
									 $subject = "Account Alert CircleCash";
									// $from = 'no-reply@sugarmenow.com';
									 $message = "
									 <html>
									 <head>
									 <title> Hello " . ucfirst($name)."</title>
									 </head>
									 <body>
									 <p>Hello " . ucfirst($name)." ,<br><br>							
									 Congratulations! Your account has been activated. Now you can use our services.<br><br>	
											 <br><br>											 
											Thanks <br>
											CircleCash Team
									 </body>
									 </html>
									 ";
									 $headers = "MIME-Version: 1.0" . "\r\n";
									 $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
									 
							//$this->session->set_flashdata('success', 'New User created successfully');
							$parameters = array(
							'to' =>$to,
							//'from' => $email,
							'subject' => $subject,
							'message' => $message
						);
						//$this->email->attach(base_url()."/uploads/qrcodes/".$qr_image);
						$this->email->sendMail($parameters);
			           echo(json_encode(array('status'=>'activated'))); 			
			}
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    
    /**
     * Page not found : error 404
     */
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'PaymentApp : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    /**
     * This function used to show login history
     * @param number $userId : This is user id
     */
    function loginHistoy($userId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $userId = ($userId == NULL ? 0 : $userId);

            $searchText = $this->input->post('searchText');
            $fromDate = $this->input->post('fromDate');
            $toDate = $this->input->post('toDate');

            $data["userInfo"] = $this->user_model->getUserInfoById($userId);

            $data['searchText'] = $searchText;
            $data['fromDate'] = $fromDate;
            $data['toDate'] = $toDate;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->loginHistoryCount($userId, $searchText, $fromDate, $toDate);

            $returns = $this->paginationCompress ( "login-history/".$userId."/", $count, 10, 3);

            $data['userRecords'] = $this->user_model->loginHistory($userId, $searchText, $fromDate, $toDate, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'PaymentApp : User Login History';
            
            $this->loadViews("loginHistory", $this->global, $data, NULL);
        }        
    }

    /**
     * This function is used to show users profile
     */
    function profile($active = "details")
    {
        $data["userInfo"] = $this->user_model->getUserInfoWithRole($this->vendorId);
        $data["active"] = $active;
        
        $this->global['pageTitle'] = $active == "details" ? 'PaymentApp : My Profile' : 'PaymentApp : Change Password';
        $this->loadViews("profile", $this->global, $data, NULL);
    }

    /**
     * This function is used to update the user details
     * @param text $active : This is flag to set the active tab
     */
    function profileUpdate($active = "details")
    {
        $this->load->library('form_validation');            
        $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
        $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
        $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]|callback_emailExists');        
        
        if($this->form_validation->run() == FALSE)
        {
            $this->profile($active);
        }
        else
        {
			if(($_FILES['profile_pic']['size'])==0)
			{
				echo $fileName = $this->input->post('pro_pic'); 
			}else{
			$uploadDirectory = "./uploads/profile-pics/";

			$errors = []; // Store all foreseen and unforseen errors here

			$fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
			 $files = $_FILES;
			 $fileName  = $_FILES['profile_pic']['name']= time().rand(100,1000).$files['profile_pic']['name'];
			 $fileType=   $_FILES['profile_pic']['type']= $files['profile_pic']['type'];
             $fileTmpName=   $_FILES['profile_pic']['tmp_name']= $files['profile_pic']['tmp_name'];
             $fileSize=   $_FILES['profile_pic']['size']= $files['profile_pic']['size'];
			 $tmp = explode('.', $fileName);
             $fileExtension = end($tmp);
             $uploadPath = $uploadDirectory . basename($fileName); 

				if (! in_array($fileExtension,$fileExtensions)) {
					$errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
					$this->session->set_flashdata('error', 'This file extension is not allowed. Please upload a JPEG or PNG file');
				}

				if ($fileSize > 2000000) {
					$errors[] = "The image file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
					 $this->session->set_flashdata('error', 'The image file is more than 2MB. Sorry, it has to be less than or equal to 2MB');
				}
                if(!empty($errors)){
					$this->session->flashdata('error'); 
				}else{
				   $didUpload = move_uploaded_file($fileTmpName, $uploadPath);                              			   
					if (!$didUpload) {
						$errors[] = "Error in image upload, try again";
						 $this->session->set_flashdata('error', 'Error in image upload, try again');
					} 
					$result = $this->user_model->getUserInfoWithRole($this->vendorId );
					unlink("./uploads/profile-pics/".$result->profile_image);	
				}	  
				   
			}
			if(empty($errors))
			{
				$name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
				$mobile = $this->security->xss_clean($this->input->post('mobile'));
				$email = strtolower($this->security->xss_clean($this->input->post('email')));
				$address = $this->security->xss_clean($this->input->post('address'));
				$city = $this->security->xss_clean($this->input->post('city'));
				$zipCode = $this->security->xss_clean($this->input->post('zipCode'));
				$state = $this->security->xss_clean($this->input->post('state'));
				$country = $this->security->xss_clean($this->input->post('country'));
				$qrFileName = $this->security->xss_clean($this->input->post('qr_file_name')); 
				$userInfo = array();		
				$userInfo = array('name'=>$name, 'email'=>$email,'mobile'=>$mobile,'profile_image'=>$fileName, 'address'=>$address, 'city'=>$city,'zipCode'=>$zipCode, 'country'=>$country, 'state'=>$state, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
				
				$result = $this->user_model->editUser($userInfo, $this->vendorId);
				
				if($result == true)
				{
					$this->session->set_userdata('name', $name);
					$this->session->set_userdata('profile_pic', $fileName);
					$this->session->set_flashdata('success', 'Profile updated successfully');
				}
				else
				{
					$this->session->set_flashdata('error', 'Profile updation failed');
				}

				redirect('profile/'.$active);
		    }else{
				redirect('profile/'.$active);
			}
        }
    }

    /**
     * This function is used to change the password of the user
     * @param text $active : This is flag to set the active tab
     */
    function changePassword($active = "changepass")
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('oldPassword','Old password','required|max_length[20]');
        $this->form_validation->set_rules('newPassword','New password','required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]|max_length[20]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->profile($active);
        }
        else
        {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
            
            $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword);
            
            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Your old password is not correct');
                redirect('profile/'.$active);
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$this->vendorId,
                                'updatedDtm'=>date('Y-m-d H:i:s'));
                
                $result = $this->user_model->changePassword($this->vendorId, $usersData);
                
                if($result > 0) { $this->session->set_flashdata('success', 'Password updation successful'); }
                else { $this->session->set_flashdata('error', 'Password updation failed'); }
                
                redirect('profile/'.$active);
            }
        }
    }

    /**
     * This function is used to check whether email already exist or not
     * @param {string} $email : This is users email
     */
    function emailExists($email)
    {
        $userId = $this->vendorId;
        $return = false;

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ $return = true; }
        else {
            $this->form_validation->set_message('emailExists', 'The {field} already taken');
            $return = false;
        }

        return $return;
    }
	
	function usernameExists()
    {
		$username= $this->input->post('username');
        $userId = $this->vendorId;
        $return = false;

        if(empty($userId)){
            $result = $this->user_model->checkUsernameExists($username);
        } else {
            $result = $this->user_model->checkUsernameExists($username, $userId);
        }

        if(empty($result)){ $return = 1; }
        else {
            //$this->form_validation->set_message('usernameExists', 'The {field} already taken');
            $return = 0;
        }

        echo $return;
    }
	/**
     * This function is used to update client bank detail
     */
	function update_bank_detail(){
		
		$account_holder = $this->input->post('holder_name');
		$bankname = $this->input->post('bankname');
		$swiftcode = $this->input->post('swiftcode');
		$routingnumber = $this->input->post('routingnumber');
		$accountnumber = $this->input->post('accountnumber');
		$user_id = $this->session->userdata('userId');

		$checkdata = $this->db->query("SELECT * FROM tbl_bank_details WHERE user_id='".$user_id."'")->num_rows();


		if($checkdata>0)
		{
			$updatedata = array(
								'account_holder'=>$account_holder,	
								'bankname'=>$bankname,
								'swiftcode'=>$swiftcode,
								'routingnumber'=>$routingnumber,
								'accountnumber'=>$accountnumber,
								);
			$this->db->where('user_id',$user_id);
			$this->db->update('tbl_bank_details',$updatedata);
			echo "2";	
		}
		else
		{
			$insertdata = array(
								'user_id'=>$user_id,
								'account_holder'=>$account_holder,
								'bankname'=>$bankname,
								'swiftcode'=>$swiftcode,
								'routingnumber'=>$routingnumber,
								'accountnumber'=>$accountnumber,
							  );
			$this->db->insert('tbl_bank_details',$insertdata);
			echo "1";
		}

	}
	
	/**
     * This function is used to get client's payment history
     */
	function payment_history()
	{
		if($this->isAdmin() != TRUE)
        {
            $this->loadThis();
        }
		 $this->global['pageTitle'] = 'PaymentApp : Payment History';
	
		$searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->userListingCount($searchText);

			$returns = $this->paginationCompress ( "payment_history/", $count, 10 );
		$type =  $this->uri->segment(2); 
		$data['paymentHistory'] = $this->user_model->getPaymentHistoryByType(base64_decode(base64_decode(base64_decode(base64_decode($type)))),$this->session->userdata('userId'));
		$this->loadViews("paymentHistory", $this->global, $data, NULL);
	}
	
	/**
     * This function is used to see all transactions in admin's panel 
     */
	function manage_transaction()
	{
		if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
		 $this->global['pageTitle'] = 'PaymentApp : Manage Transaction';
		$data['manageTransaction'] = $this->user_model->getAllTransaction();
		$this->loadViews("manageTransaction", $this->global, $data, NULL);
	}
	
	/**
     * This function is used to update update comission formula
     */
	function manage_comission()
	{
		if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
		if($this->input->post('type')==1)
		{
			$comissionInfo = array('id'=>$this->input->post('id'),'comission_percentage'=>$this->input->post('comission_info'));
			$data['manageComission'] = $this->user_model->UpdateCommisionpercentage($type=1,$comissionInfo);
			if($data['manageComission']!=0){
				$this->session->set_flashdata('success','Successfully Updated');
			}
			
		}
		$this->global['pageTitle'] = 'PaymentApp : Manage Comission';	  
		$data['manageComission'] = $this->user_model->getComissionPercentage();
		$this->loadViews("manageComission", $this->global, $data, NULL);		
	}
	
	/**
     * This function is used to transfer fund in clien's account.
     */
	 function pay_to_merchant()
	 {
		$this->global['pageTitle'] = 'PaymentApp : Manage Comission';	  
		$data['manageTransaction'] = $this->user_model->getAllTransaction();
		$data['getComission'] = $this->user_model->getComissionPercentage();
		$this->loadViews("payToMerchant", $this->global, $data, NULL);	
	 }
	 function overviewDailyPayment()
	 {
		 $this->global['pageTitle'] = 'PaymentApp : Overview of Daily Payment';	  
		$data['overviewDailyPayment'] = $this->user_model->overviewDailyPayment();
		$data['getComission'] = $this->user_model->getComissionPercentage();
		$this->loadViews("overviewDailyPayment", $this->global, $data, NULL);	
	 }
	 function payToMerchantByAdmin()
	 {
		 $amount = $this->input->post('amount');
		 $aIId = $this->input->post('ai_id');
		 $transId = $this->input->post('transaction_Id');
		 $transDate =$this->input->post('trans_date');
		 $userInfo =array('paidToMerchantAmount'=>$amount,'paidByAdminTransactionId'=>$transId,'paid_by_admin_at'=>$transDate,'paid_by_admin'=>1);
		 $data = $this->user_model->updateClientAdminPay($userInfo,$aIId);
		 if($data==1)
			 echo 1;
		 else
			 echo 2;
	 }
	 
}

?>