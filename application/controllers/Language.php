<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class User extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->isLoggedIn();   
		$this->load->library('email');
        $this->load->helper('array');
		$lang= ($this->session->userdata('lang')) ?
		$this->session->userdata('lang') : 'english';
        $this->lang->load('trans',$lang);
    }
	
	
	public function english(){
		$this->session->unset_userdata('lang');
		$this->session->set_userdata('lang','english');
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		
	}
	public function german(){
		$this->session->unset_userdata('lang');
		$this->session->set_userdata('lang','french');
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		
	}
	public function german(){
		$this->session->unset_userdata('lang');
		$this->session->set_userdata('lang','arabic');
		header('Location: ' . $_SERVER['HTTP_REFERER']);
		
	}
	
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'CodeInsect : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
    }
    
    /**
     * This function is used to load the user list
     */
    
}

?>