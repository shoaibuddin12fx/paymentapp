<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Api-Key, User-Name, Authorization, If-Modified-Since, Cache-Control, Pragma");
require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Webservices (AppController)
 * Webservices Class to control all user related operations.
 * @author : Jayendra Narayan Singh
 * @version : 0.1
 * @since : 18 February 2019
 */
class Webservices extends CI_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model'); 
        $this->load->model('login_model'); 
		$this->load->library('email');
        $this->load->helper('array');
		$lang= ($this->session->userdata('lang')) ?
		$this->session->userdata('lang') : 'english';
        $this->lang->load('trans',$lang);
    }
	
	public function loginMe()
    {       $request = json_decode(file_get_contents('php://input'),true);
	        if(empty($request['email']) || empty($request['password']))
			{
				$data= array("status"=>0,"msg"=>"Invalid Request");
				$this->response($data);
				exit();				
			}     
		    $email = strtolower($this->security->xss_clean(trim($request['email'])));
            $password = $this->security->xss_clean(trim($request['password']));
            $result = $this->login_model->loginMe($email, $password);
            
            if(!empty($result))
            {
                $lastLogin = $this->login_model->lastLoginInfo($result->userId);
            if($result->roleId != 3)
			{
				$data= array("status"=>0,"msg"=>"You are not authorize to login from here");
				$this->response($data);
				exit();
			}
			if($result->isDeactivated == 1)
			{
				$data= array("status"=>0,"msg"=>"Your account is deactivated");
				$this->response($data);
				exit();
			}
                $sessionArray = array('userId'=>$result->userId,                    
                                        'role'=>$result->roleId,
                                        'roleText'=>$result->role,
                                        'name'=>$result->name,
										'email'=>$result->email,
										'profileImage'=>$result->profile_image,
                                        'lastLogin'=> $lastLogin->createdDtm,
                                        'isLoggedIn' => TRUE
                                );                
                $loginInfo = array("userId"=>$result->userId, "sessionData" => json_encode($sessionArray), "machineIp"=>$_SERVER['REMOTE_ADDR'], "userAgent"=>getBrowserAgent(), "agentString"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());
                $this->login_model->lastLogin($loginInfo); 
				$data= array("status"=>1,"data"=>$sessionArray);
				$this->response($data);
            }
            else
            {
                $data= array("status"=>0,"msg"=>"Email or Password mismatch");
				$this->response($data);
				exit();	
            }
        
    }
	function addNewUser()
    {
		    $request = json_decode(file_get_contents('php://input'),true);
			if(empty($request['name']) || empty($request['email']) || empty($request['password']) || empty($request['mobile']) ||empty($request['userName']))
			{
				$data= array("status"=>0,"msg"=>"Invalid Request");
				$this->response($data);
				exit();				
			}          					  
			$name = ucwords(strtolower($this->security->xss_clean(trim($request['name']))));
			$userName = $this->security->xss_clean(trim($request['name']));
			$this->userNameExists($userName);
			$email = strtolower($this->security->xss_clean(trim($request['email'])));
			$this->emailExists($email); 
			$password = $this->security->xss_clean($request['password']);
			$mobile = $this->security->xss_clean($request['mobile']);
			$userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>3, 'name'=> $name,'mobile'=>$mobile, 'createdBy'=>3, 'createdDtm'=>date('Y-m-d H:i:s'));
			
			$this->load->model('user_model');
			$result = $this->user_model->addNewUser($userInfo);
			
			if($result > 0)
			{
						 $to = $email; 
						 $subject = "Registration Successfull";
						 $from = 'no-reply@sugarmenow.com';
						 $message = "
						 <html>
						 <head>
						 <title> Hello " . ucfirst($name)."</title>
						 </head>
						 <body>
						 <p>Hello " . ucfirst($name)." ,<br><br>							
						 Welcome to PaymentApp<br>
						 Your account has been created. Following are your Login Credentials<br>	
								 Username:- " .$email.
								 "<br>Password:- " .$password.
								 "<br>			
								Thanks <br>
								PaymentApp Team
						 </body>
						 </html>
						 ";
						 $headers = "MIME-Version: 1.0" . "\r\n";
						 $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
						 $headers .= 'From: '.$from."\r\n".
							   'Reply-To: '.$from."\r\n" ;
				$data= array("status"=>1,"msg"=>"User Created Successfully");				
				$parameters = array(
				'to' =>$to,
				//'from' => $email,
				'subject' => $subject,
				'message' => $message
			);
			$this->email->sendMail($parameters);
			$this->response($data);
			}
			else
			{
				$data= array("status"=>0,"msg"=>"User Creation Failed! Try again");
				$this->response($data);
			}
            
        
    }
	
	function changePassword()
	{
		    $request = json_decode(file_get_contents('php://input'),true);
			if(empty($request['oldPassword']) || empty($request['newPassword']) || empty($request['userId']))
			{
				$data= array("status"=>0,"msg"=>"Invalid Request");
				$this->response($data);
				exit();				
			}     
		    $oldPassword = trim($request['oldPassword']);
            $newPassword = trim($request['newPassword']);
            $userId = trim($request['userId']);
            $this->checkUserExist($userId);
            $resultPas = $this->user_model->matchOldPassword($userId, $oldPassword);    
            if(empty($resultPas))
            {
                $data= array("status"=>0,"msg"=>"Your old password is incorrect");
				$this->response($data);
				exit();	
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$userId,
                                'updatedDtm'=>date('Y-m-d H:i:s'));
                
                $result = $this->user_model->changePassword($userId, $usersData);
                
                if($result > 0) { 
				$data= array("status"=>1,"msg"=>"Password has been updated successfully");
				$this->response($data);
				exit(); 
				}
                else { 
					$data= array("status"=>0,"msg"=>"Password updation failed");
					$this->response($data);
					exit(); 
				}
	        }
	}
	
	function getBankDetails()
	{
		    $request = json_decode(file_get_contents('php://input'),true);				
            if(empty($request['userId']))
			{
				$data= array("status"=>0,"msg"=>"Invalid Request");
				$this->response($data);
				exit();				
			}
			$userId = trim($request['userId']);
			  $this->checkUserExist($userId);
			  $bank_detail = $this->user_model->getBankDetails($userId);
			  $data= array("status"=>1,"data"=>$bank_detail);
			  $this->response($data);
	}
	
	function updateBankDetails()
	{
		$request = json_decode(file_get_contents('php://input'),true);				
            if(empty($request['holder_name']) || empty($request['bankname']) || empty($request['swiftcode']) || empty($request['routingnumber']) || empty($request['accountnumber']) || empty($request['userId']))
			{
				$data= array("status"=>0,"msg"=>"Invalid Request");
				$this->response($data);
				exit();				
			}
		$account_holder = trim($request['holder_name']);
		$bankname = trim($request['bankname']);
		$swiftcode = trim($request['swiftcode']);
		$routingnumber = trim($request['routingnumber']);
		$accountnumber = trim($request['accountnumber']);
		$userId = trim($request['userId']);
        $this->checkUserExist($userId);
		$checkdata = $this->db->query("SELECT * FROM tbl_bank_details WHERE user_id='".$userId."'")->num_rows();
		if($checkdata>0)
		{
			$updatedata = array(
								'account_holder'=>$account_holder,	
								'bankname'=>$bankname,
								'swiftcode'=>$swiftcode,
								'routingnumber'=>$routingnumber,
								'accountnumber'=>$accountnumber,
								);
			$this->db->where('user_id',$userId);
			$this->db->update('tbl_bank_details',$updatedata);
			$data= array("status"=>1,"msg"=>"Bank details updated successfully");	
		}
		else
		{
			$insertdata = array(
								'user_id'=>$userId,
								'account_holder'=>$account_holder,
								'bankname'=>$bankname,
								'swiftcode'=>$swiftcode,
								'routingnumber'=>$routingnumber,
								'accountnumber'=>$accountnumber,
							  );
			$this->db->insert('tbl_bank_details',$insertdata);
			$data= array("status"=>1,"msg"=>"Bank details inserted successfully");
		}
		$this->response($data);
	}
	
	function getUserDetails()
	{
		$request = json_decode(file_get_contents('php://input'),true);				
            if(empty($request['userId']))
			{
				$data= array("status"=>0,"msg"=>"Invalid Request");
				$this->response($data);
				exit();				
			}
			$userId = trim($request['userId']);
		$this->checkUserExist($userId);
		$result = $this->user_model->getUserInfoWithRole($userId);
		$data= array("status"=>1,"data"=>$result);
		$this->response($data);
	}
	
	function updateUserDetails()
	{
		$request = json_decode(file_get_contents('php://input'),true);				
            if(empty($request['userId']) || empty($request['name']) || empty($request['mobile']) || empty($request['email']) || empty($request['address'])  || empty($request['city']) || empty($request['state']) || empty($request['country'])|| empty($request['zipCode']))
			{
				$data= array("status"=>0,"msg"=>"Invalid Request");
				$this->response($data);
				exit();				
			}
			$userId = ucwords(strtolower(trim($request['userId'])));
			$name = trim($request['name']);
			$mobile = trim($request['mobile']);
			$email = strtolower($this->security->xss_clean(trim($request['email'])));
			$address = trim($request['address']);
			$city = trim($request['city']);
			$state = trim($request['state']);
			$country = trim($request['country']);
			$zipCode = trim($request['zipCode']);
		$this->checkUserExist($userId);  		
		$userInfo = array('name'=>$name, 'email'=>$email,'mobile'=>$mobile,'address'=>$address, 'city'=>$city, 'country'=>$country, 'state'=>$state,'zipCode'=>$zipCode, 'updatedBy'=>$userId, 'updatedDtm'=>date('Y-m-d H:i:s'));            
		$result = $this->user_model->editUser($userInfo, $userId);		
		if($result == true)
		{
			$data= array("status"=>1,"msg"=>"Profile has been updated successfully");
			$this->response($data);
			exit();	
		}
		else
		{
			$data= array("status"=>0,"msg"=>"Profile updation failed, try again");
			$this->response($data);
			exit();	
		}

	}
	function changeProfilePicture()
	{
		
		  $request = json_decode(file_get_contents('php://input'),true);
		  if(empty($request['userId']) || empty($request['profileImage']) || empty($request['profileImageName']))
		  {
			 $data= array("status"=>0,"msg"=>"Invalid Request");
			 $this->response($data);
			 exit();	
		  }
		  $userId =  $request['userId'];
		  $image  =  $request['profileImage'];
	      $fname  =  $request['profileImageName'];
		  $this->checkUserExist($userId); 
		  list($width,$height) = getimagesize($image);
				    $thumb = imagecreatetruecolor(420,300);
				    $source = imagecreatefromjpeg($image);
					if($source){
						imagecopyresampled($thumb,$source,0,0,0,0,420,300,$width,$height);					
						$taxi_image=md5(time()).$fname;
						  if(imagejpeg($thumb,"./uploads/profile-pics/".$taxi_image, 100))
						  {
							$result = $this->user_model->getUserInfoWithRole($userId);
							unlink("./uploads/profile-pics/".$result->profile_image); 
							$this->db->where('userId', $userId);
                            $this->db->update('tbl_users', array("profile_image"=>$taxi_image));
						    $data= array("status"=>1,"msg"=>"Profile image changed successfully");
						    $this->response($data);
						  }
					}else
					{
						 $data=array("status"=>"0","msg"=>"Invalid image type!");
					}	  		
	}
	function getPaymentHistory()
	{
		  $request = json_decode(file_get_contents('php://input'),true);
		  if(empty($request['userId']))
		  {
			 $data= array("status"=>0,"msg"=>"Invalid Request");
			 $this->response($data);
			 exit();	
		  }
		  $userId =  trim($request['userId']);
		  $this->checkUserExist($userId); 
		  $result = $this->user_model->getPaymentHistoryCustomer($userId);
		  $data= array("status"=>1,"data"=>$result);
		  $this->response($data);
		  
	}
	function customerForgotPassword()
	{
		$string = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string_shuffled = str_shuffle($string);
        $password = substr($string_shuffled, 1, 6);		
        $authToken = bin2hex(random_bytes(16));	
		$request = json_decode(file_get_contents('php://input'),true);
		  if(empty($request['userName']))
		  {
			 $data= array("status"=>0,"msg"=>"Invalid Request");
			 $this->response($data);
			 exit();	
		  }
          $userName = trim($request['userName']);          
            if($this->user_model->checkUsernameExists($userName))
            {
				$result = $this->user_model->getUserInfoWithRole($userName);
				$email= $result->email; 
                $encoded_email = urlencode($email);
                
                $this->load->helper('string');
                $data['email'] = $email;
                $data['activation_id'] = $password;
                $data['auth_token'] = $authToken;
                $data['createdDtm'] = date('Y-m-d H:i:s');
                $data['agent'] = getBrowserAgent();
                $data['client_ip'] = $this->input->ip_address();
                
                $save = $this->login_model->resetPasswordUser($data);                
                
                if($save)
                {
                    $data1['message'] = 
					"
									 <html>
									 <head>
									 <title> Reset Your Password</title>
									 </head>
									 <body>
									 <p>Hello " . $data['email']." ,<br><br>							
									
									 You have requested to reset your password. This is your otp to reset password<br>	
											<h2>".$data['activation_id']."</h2><br>
											Use the otp in App to reset password
											 <br><br>			
											Thanks <br>
											CircleCash Team
									 </body>
									 </html>
									 ";					
                    $userInfo = $this->login_model->getCustomerInfoByEmail($email);

                    if(!empty($userInfo)){
						$parameters = array(
							'to' =>$userInfo->email,
							//'from' => $email,
							'subject' => "Reset Password",
							'message' => $data1['message']
						);
                        //$data1["name"] = $userInfo->name;
                        //$data1["email"] = $userInfo->email;
                        //$data1["message"] = "Reset Your Password";
                    }

                    $sendStatus = $this->email->sendMail($parameters);;

                    if($sendStatus){
						$data= array("authToken"=>$authToken,"userName"=>$userName);
                        $status = "send";
						$data= array("status"=>1,"msg"=>"An Otp is sent to your Email plz! check","data"=>$data);                     
                    } else {
                        $status = "notsend";
						$data= array("status"=>0,"msg"=>"Email has been failed, try again.");                      
                    }
                }
                else
                {
                    $status = 'unable';
					$data= array("status"=>0,"msg"=>"It seems an error while sending your details, try again."); 
                }
            }
            else
            {
                $status = 'invalid';
                $data= array("status"=>0,"msg"=>"You are not registered with us."); 
            }
		$this->response($data);				
	}
	
	function createPasswordCustomer()
    {
        $status = '';
        $message = '';      
        $request = json_decode(file_get_contents('php://input'),true);
		  if(empty($request['userName']) || empty($request['activationCode']) || empty($request['password']) || empty($request['cpassword']) || empty($request['authToken']))
		  {
			 $data= array("status"=>0,"msg"=>"Invalid Request");
			 $this->response($data);
			 exit();	
		  }
          $userName = trim($request['userName']);   
          $activation_id = trim($request['activationCode']);   
          $authToken = trim($request['authToken']);   
          $password = trim($request['password']);   
          $cpassword = trim($request['cpassword']); 
          $result = $this->user_model->getUserInfoWithRole($userName);
		  $email= $result->email;		  
            // Check activation id in database
			if($password === $cpassword)
			{
				$is_correct = $this->login_model->checkActivationDetailsMobile($email, $activation_id, $authToken);				
				if($is_correct == 1)
				{                
					$this->login_model->createPasswordUser($email, $password);
					
					$status = 'success';				
					$data= array("status"=>1,"msg"=>"Password reset successfully.");
				}
				else
				{
					$status = 'error';
					$data= array("status"=>0,"msg"=>"The otp has been expired.");
				}
            }else{
				$data= array("status"=>0,"msg"=>"Both passwords are not same.");
			}
            $this->response($data);
        
    }
	
	
	function emailExists($email)
	{
		$result = $this->user_model->checkEmailExists($email);
		if(empty($result)){ $return = true; }
		else {
			    $data= array("status"=>0,"msg"=>"Email already Exist! choose another one");
				$this->response($data);
				exit();			
		}
	}
	function userNameExists($userName)
	{
		$result = $this->user_model->checkUsernameExists($userName);
		if(empty($result)){ $return = true; }
		else {
			    $data= array("status"=>0,"msg"=>"Username already Exist! choose another one");
				$this->response($data);
				exit();			
		}
	}
	function checkUserExist($userId)
	{
		$result = $this->user_model->getUserInfoById($userId);
		if(empty($result))
		{
			$data= array("status"=>0,"msg"=>"User doesn't exist");
			$this->response($data);
			exit();	
		}
	}
	public function response($data)
	{
		print_r(json_encode($data));
		exit;
	}	
	
}

?>